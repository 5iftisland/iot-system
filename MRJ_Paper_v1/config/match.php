<?PHP

//include "address.php";
//include "files.php";

/**
        功能：  防止SQL Injection
        參數：  string or array
        return  string or array
*/
function SQLInjection($content){
        //如果magic_quotes_gpc=Off，那麽就開始處理
        if (!get_magic_quotes_gpc()) {
                //判斷$content是否爲陣列
                if (is_array($content)) {
                        //如果$content是陣列，那麽就處理它的每一個單無
                        foreach ($content as $key=>$value) {
                                $content[$key] = addslashes($value);
                        }
                }
                else{
                        //如果$content不是陣列，那麽就僅處理一次
                        addslashes($content);
                }
        }
        return $content;
}

/**
        功能：  電話號碼驗證
        參數：  phone
        return  boolean
*/
function is_phone($phone){
//0910-626520, 0開頭,不可為0,10-626520
//06-2289894,0開頭,不可為0,-2289894
//      if(!preg_match("/^[0][1-9][0-9]{6,8}$/", $phone) || !preg_match("/^[0][1-9][0-9]{0,2}[-][0-9]{6,8}$/", $phone) || strlen($phone) < 10 || strlen($phone) > 12){
        if(!preg_match("/^[0][1-9][0-9]{6,8}$/", $phone) && !preg_match("/^[0][1-9][0-9]{0,2}[-][0-9]{6,8}$/", $phone) ){
                return false;
        }
        else return true;
}

/**
        功能：  信箱驗證
        參數：  $email,$checkDNS
        return  boolean
*/
function is_email ($email, $checkDNS = false) {
    $index = strrpos($email,'@');    //帳號長度

    if ($index === false)       return false;   //  No at-sign
//    if ($index === 0)           return false;   //  No local part//帳號長度
    if ($index < 2  || $index > 64)           return false;   //  No local part//帳號太短太長
//    if ($index > 64)            return false;   //  Local part too long//帳號太長

    $localPart      = substr($email, 0, $index);   //帳號部分
    $domain         = substr($email, $index + 1);   //網址部分
    $domainLength   = strlen($domain);        //網址長度

    if ($domainLength === 0)    return false;   //  No domain part
    if ($domainLength > 255)    return false;   //  Domain part too long

    //  Let's check the local part for RFC compliance...
    //
    //  Period (".") may...appear, but may not be used to start or end the
    //  local part, nor may two or more consecutive periods appear.
    //      (http://tools.ietf.org/html/rfc3696#section-3)

    if (preg_match('/^\\.|\\.\\.|\\.$/', $localPart) > 0)               return false;   //  Dots in wrong place

    //  Any ASCII graphic (printing) character other than the
    //  at-sign ("@"), backslash, double quote, comma, or square brackets may
    //  appear without quoting.  If any of that list of excluded characters
    //  are to appear, they must be quoted
    //      (http://tools.ietf.org/html/rfc3696#section-3)
    if (preg_match('/^"(?:.)*"$/', $localPart) > 0) {
        //  Local part is a quoted string
        if (preg_match('/(?:.)+[^\\\\]"(?:.)+/', $localPart) > 0)   return false;   //  Unescaped quote character inside quoted string
    }
        else {
        if (preg_match('/[ @\\[\\]\\\\",]/', $localPart) > 0)
            //  Check all excluded characters are escaped
            $stripped = preg_replace('/\\\\[ @\\[\\]\\\\",]/', '', $localPart);

                //if (preg_match('/[ @\\[\\]\\\\",]/', $stripped) > 0)        return false;   //  Unquoted excluded characters
    }

    //  Now let's check the domain part...

    //  The domain name can also be replaced by an IP address in square brackets
    //      (http://tools.ietf.org/html/rfc3696#section-3)
    //      (http://tools.ietf.org/html/rfc5321#section-4.1.3)
    //      (http://tools.ietf.org/html/rfc4291#section-2.2)
    if (preg_match('/^\\[(.)+]$/', $domain) === 1) {
        //  It's an address-literal
        $addressLiteral = substr($domain, 1, $domainLength - 2);
        $matchesIP      = array();

        //  Extract IPv4 part from the end of the address-literal (if there is one)
        if (preg_match('/\\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/', $addressLiteral, $matchesIP) > 0) {
            $index = strrpos($addressLiteral, $matchesIP[0]);

            if ($index === 0) {
                //  Nothing there except a valid IPv4 address, so...
                return true;
            } else {
                //  Assume it's an attempt at a mixed address (IPv6 + IPv4)
                if ($addressLiteral[$index - 1] !== ':')            return false;   //  Character preceding IPv4 address must be ':'
                if (substr($addressLiteral, 0, 5) !== 'IPv6:')      return false;   //  RFC5321 section 4.1.3

                $IPv6 = substr($addressLiteral, 5, ($index ===7) ? 2 : $index - 6);
                $groupMax = 6;
            }
        } else {
            //  It must be an attempt at pure IPv6
            if (substr($addressLiteral, 0, 5) !== 'IPv6:')          return false;   //  RFC5321 section 4.1.3
            $IPv6 = substr($addressLiteral, 5);
            $groupMax = 8;
        }

        $groupCount = preg_match_all('/^[0-9a-fA-F]{0,4}|\\:[0-9a-fA-F]{0,4}|(.)/', $IPv6, $matchesIP);
        $index      = strpos($IPv6,'::');

        if ($index === false) {
            //  We need exactly the right number of groups
            if ($groupCount !== $groupMax)                          return false;   //  RFC5321 section 4.1.3
        } else {
            if ($index !== strrpos($IPv6,'::'))                     return false;   //  More than one '::'
            $groupMax = ($index === 0 || $index === (strlen($IPv6) - 2)) ? $groupMax : $groupMax - 1;
            if ($groupCount > $groupMax)                            return false;   //  Too many IPv6 groups in address
        }

        //  Check for unmatched characters
        array_multisort($matchesIP[1], SORT_DESC);
        if ($matchesIP[1][0] !== '')                                    return false;   //  Illegal characters in address

        //  It's a valid IPv6 address, so...
        return true;
    } else {
        //  It's a domain name...

        //  The syntax of a legal Internet host name was specified in RFC-952
        //  One aspect of host name syntax is hereby changed: the
        //  restriction on the first character is relaxed to allow either a
        //  letter or a digit.
        //      (http://tools.ietf.org/html/rfc1123#section-2.1)
        //
        //  NB RFC 1123 updates RFC 1035, but this is not currently apparent from reading RFC 1035.
        //
        //  Most common applications, including email and the Web, will generally not permit...escaped strings
        //      (http://tools.ietf.org/html/rfc3696#section-2)
        //
        //  Characters outside the set of alphabetic characters, digits, and hyphen MUST NOT appear in domain name
        //  labels for SMTP clients or servers
        //      (http://tools.ietf.org/html/rfc5321#section-4.1.2)
        //
        //  RFC5321 precludes the use of a trailing dot in a domain name for SMTP purposes
        //      (http://tools.ietf.org/html/rfc5321#section-4.1.2)
        $matches    = array();
        $groupCount = preg_match_all('/(?:[0-9a-zA-Z][0-9a-zA-Z-]{0,61}[0-9a-zA-Z]|[a-zA-Z])(?:\\.|$)|(.)/', $domain, $matches);
        $level      = count($matches[0]);

        if ($level == 1)                                            return false;   //  Mail host can't be a TLD

        $TLD = $matches[0][$level - 1];
        if (substr($TLD, strlen($TLD) - 1, 1) === '.')              return false;   //  TLD can't end in a dot
        if (preg_match('/^[0-9]+$/', $TLD) > 0)                     return false;   //  TLD can't be all-numeric

        //  Check for unmatched characters
        array_multisort($matches[1], SORT_DESC);
        if ($matches[1][0] !== '')                          return false;   //  Illegal characters in domain, or label longer than 63 characters

        //  Check DNS?
        if ($checkDNS && function_exists('checkdnsrr')) {
            if (!(checkdnsrr($domain, 'A') || checkdnsrr($domain, 'MX'))) {
                return false;   //  Domain doesn't actually exist
            }
        }

        //  Eliminate all other factors, and the one which remains must be the truth.
        //      (Sherlock Holmes, The Sign of Four)
        return true;
    }
}

/**
        功能：  驗證日期格式
        參數：  YYYY-MM-DD or YYYY/MM/DD
        return  boolean
*/
function is_DateISO($date){
        if(strpos($date,"-")) $data_array=explode("-",$date);
        else if(strpos($date,"/")) $data_array=explode("/",$date);
        if(isset($data_array)) return @checkdate($data_array[1], $data_array[2], $data_array[0]);
        else return false;
}

/**
        功能：  密碼驗算
        參數：  $pwd    密碼
        return  string
*/
function GetPassword($pwd){
        return md5(urlencode($pwd));
}

/**
        功能：  比對兩字串是否相同
        參數：  string
        return  boolean
*/
function match_strcmp($str1,$str2){
        if(strcmp($str1,$str2)==0) return true;
        else return false;
}

function dierr($str){
        die('[{"msg":'.$str.'}]');
//      die('[{"msg":"'.$str.'"}]');
}

function dierrmsg($str,$str1){
        die('[{"msg":'.$str.',"message":"'.$str1.'"}]');
//      die('[{"msg":'.$str.'}{"message":"'.$str1.'"}]');
//      die('[{"msg":"'.$str.'"}{"message":"'.$str1.'"}]');
}

function dierrmsg3($str,$str1,$str2){
//      die('[{"msg":'.$str.'}{"message":"'.$str1.'"}]');
//error die('[{"msg":"'.$str.'"}{"code":"'.$str1.'"}{"message":"'.$str2.'"}]');
//      die('[{"msg":"'.$str.'","code":"'.$str1.'","message":"'.$str2.'"}]');
        die('[{"msg":'.$str.',"code":'.$str1.',"message":"'.$str2.'"}]');
}

/**
        功能：  強制刪除資料夾
        參數：  $dir    路徑
        return  boolean
*/
function deleteDirectory($dir) {
    if (!file_exists($dir)) return true;
    if (!is_dir($dir) || is_link($dir)) return unlink($dir);
        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') continue;
            if (!deleteDirectory($dir . "/" . $item)) {
                chmod($dir . "/" . $item, 0777);
                if (!deleteDirectory($dir . "/" . $item)) return false;
            };
        }
        return rmdir($dir);
}

?>