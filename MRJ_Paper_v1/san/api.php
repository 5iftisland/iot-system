<?php
$lifeTime = 3600 * 24 * 30;    ////// 30天
session_set_cookie_params($lifeTime);
session_start();
require_once("config.php");
require_once("email.php");
require_once("mosquitto.php");
header('Content-Type: text/html; charset=utf-8');
//$post=$get;

switch($post['mode']){
///////////////////////////////////////////////////////////////////
        /*
        功能:消費者更改資料
        */
        case "consumer_setting":
                if(isset($post['account'])&&isset($post['password'])&&isset($post['lastname'])&&isset($post['firstname'])&&isset($post['birthday'])&&isset($post['email'])&&isset($post['phone'])&&isset($post['IMEI'])){
                        if(strlen($post['account'])<5 || strlen($post['account'])>16) dierrmsg3(-1,4,"帳號短於6字或大於16字"); //帳號太短
                        if(strlen($post['password'])<5 || strlen($post['password'])>16) dierrmsg3(-1,5,"密碼短於6字或大於16字"); //密碼格式錯誤
                        if(!is_DateISO($post['birthday'])) dierrmsg3(-1,13,"日期格式錯誤"); //日期格式錯誤
                        if(!is_email($post['email'])) dierrmsg3(-1,14,"信箱格式:name@ksu.com"); //信箱格式錯誤
// 未來APP才會用到的欄位
                        if(strlen($post['IMEI'])!=15) dierrmsg3(-1,17,"請用手機註冊"); //未用手機註冊
                        if(!is_phone($post['phone'])) dierrmsg3(-1,10,"電話格式:區碼-電話號碼"); //電話格式錯誤

                        $account=$post['account'];
                        $password=GetPassword($post['password']);
                        $lastname=$post['lastname'];
                        $firstname=$post['firstname'];
                        $birthday=$post['birthday'];
                        $email=$post['email'];
                        $phone=$post['phone'];
                        $IMEI=GetPassword($post['IMEI']);

                        //帳號是否有註冊
                        $row=ASQL($db,"SELECT * FROM consumer  WHERE account='".$account."' ");
                        if($row > 0) {
                                if($row['account']==$account && $row['password']==$password && $row['lastname']==$lastname && $row['firstname']==$firstname && $row['birthday']==$birthday && $row['email']==$email && $row['phone']==$phone && $row['IMEI']==$IMEI )   dierr(0); //沒有修改
                                elseif($row['account']==$account && $row['password']==$password && $row['lastname']==$lastname && $row['firstname']==$firstname && $row['birthday']==$birthday && $row['email']==$email && $row['phone']==$phone ){
                                        $rs=$db->exec("UPDATE  consumer SET IMEI = '".$IMEI."', time =  now() WHERE  account='".$account."' and password='".$password."' and  lastname='".$lastname."' and firstname='".$firstname."' and birthday='".$birthday."' and email='".$email."' and phone='".$phone."'  ");
                                }
                                elseif($row['account']==$account && $row['password']==$password && $row['lastname']==$lastname && $row['firstname']==$firstname && $row['birthday']==$birthday && $row['email']==$email && $row['IMEI']==$IMEI){
                                        $rs=$db->exec("UPDATE  consumer SET phone = '".$phone."', time =  now() WHERE  account='".$account."' and password='".$password."' and  lastname='".$lastname."' and firstname='".$firstname."' and birthday='".$birthday."' and email='".$email."'  and IMEI='".$IMEI."' ");
                                }
                                elseif($row['account']==$account && $row['password']==$password && $row['lastname']==$lastname && $row['firstname']==$firstname && $row['birthday']==$birthday && $row['phone']==$phone && $row['IMEI']==$IMEI ){
                                        $rs=$db->exec("UPDATE  consumer SET email = '".$email."', time =  now() WHERE  account='".$account."' and password='".$password."' and  lastname='".$lastname."' and firstname='".$firstname."' and birthday='".$birthday."'  and phone='".$phone."' and IMEI='".$IMEI."' ");
                                }
                                elseif($row['account']==$account && $row['password']==$password && $row['lastname']==$lastname && $row['firstname']==$firstname && $row['email']==$email && $row['phone']==$phone && $row['IMEI']==$IMEI ){
                                        $rs=$db->exec("UPDATE  consumer SET birthday = '".$birthday."', time =  now() WHERE  account='".$account."' and password='".$password."' and  lastname='".$lastname."' and firstname='".$firstname."' and  email='".$email."' and phone='".$phone."' and IMEI='".$IMEI."' ");
                                }
                                elseif($row['account']==$account && $row['password']==$password && $row['lastname']==$lastname && $row['birthday']==$birthday && $row['email']==$email && $row['phone']==$phone && $row['IMEI']==$IMEI ){
                                        $rs=$db->exec("UPDATE  consumer SET firstname = '".$firstname."', time =  now() WHERE  account='".$account."' and password='".$password."' and  lastname='".$lastname."' and birthday='".$birthday."' and email='".$email."' and phone='".$phone."' and IMEI='".$IMEI."' ");
                                }
                                elseif($row['account']==$account && $row['password']==$password && $row['firstname']==$firstname && $row['birthday']==$birthday && $row['email']==$email && $row['phone']==$phone && $row['IMEI']==$IMEI ){
                                        $rs=$db->exec("UPDATE  consumer SET lastname = '".$lastname."', time =  now() WHERE  account='".$account."' and password='".$password."' and firstname='".$firstname."' and birthday='".$birthday."' and email='".$email."' and phone='".$phone."' and IMEI='".$IMEI."' ");
                                }
                                elseif($row['account']==$account && $row['lastname']==$lastname && $row['firstname']==$firstname && $row['birthday']==$birthday && $row['email']==$email && $row['phone']==$phone && $row['IMEI']==$IMEI ){
                                        $rs=$db->exec("UPDATE  consumer SET password = '".$password."', time =  now() WHERE  account='".$account."'  and  lastname='".$lastname."' and firstname='".$firstname."' and birthday='".$birthday."' and email='".$email."' and phone='".$phone."' and IMEI='".$IMEI."' ");
                                }       else      dierrmsg3(-1,8,"每次只能改一項");
//                              dierrmsg3(0,0,"OK");
//      die('[{"msg":'.$str.',"code":"'.$str1.'","message":"'.$str2.'"}]');
                                dierr(0);
//      die('[{"msg":'.$str.'}]');
                        }
                        else                    //      dierr(0);
                                dierrmsg3(-1,9,"資料有誤請重試");
                }
                else{
//                              dierr(0);
                        dierrmsg3(-1,3,"資料不可空白"); //資料未填寫完整
                }
        break;

        /*
        功能:消費者註冊
        */
        case "consumer_register":
                if(isset($post['account'])&&isset($post['password'])&&isset($post['lastname'])&&isset($post['firstname'])&&isset($post['birthday'])&&isset($post['city'])&&isset($post['address'])&&isset($post['email'])&&isset($post['phone'])&&isset($post['IMEI'])){
                        if(strlen($post['account'])<6 || strlen($post['account'])>16) dierrmsg3(-1,4,"帳號短於6字或大於16字"); //帳號太短
                        if(strlen($post['password'])<6 || strlen($post['password'])>16) dierrmsg3(-1,5,"密碼短於6字或大於16字"); //密碼格式錯誤
                        if(!is_DateISO($post['birthday'])) dierrmsg3(-1,13,"日期格式錯誤"); //日期格式錯誤
//                      if(!is_address($post['address'])) dierrmsg3(-1,"地址格式:台南市北區公園路"); //不正確地址
                        if(!is_email($post['email'])) dierrmsg3(-1,14,"信箱格式:name@ksu.com"); //信箱格式錯誤
// 未來APP才會用到的欄位
//                      if(strlen($post['IMEI'])!=15) dierrmsg3(-1,17,"請用手機註冊"); //未用手機註冊
//                      if(!is_city($post['city'])) dierrmsg3(-1,"不正確縣市"); //不正確縣市
                        if(!is_phone($post['phone'])) dierrmsg3(-1,10,"電話格式:區碼-電話號碼"); //電話格式錯誤

                        $account=$post['account'];
                        $password=GetPassword($post['password']);
                        $lastname=$post['lastname'];
                        $firstname=$post['firstname'];
                        $birthday=$post['birthday'];
                        $city=$post['city'];
//                      $address=get_address($post['address']);
//                      $address=$address['address'];
                        $address=$post['address'];
                        $email=$post['email'];
                        $phone=$post['phone'];
                        $IMEI=GetPassword($post['IMEI']);
                        $dispname=$lastname.$firstname;
                                $sql="INSERT INTO regist_detail (account, password, lastname, firstname, birthday, city, address, phone, email) VALUES ('".$account."', '".$password."', '".$lastname."', '".$firstname."', '".$birthday."', '".$city."', '".$address."', '".$phone."', '".$email."' )";
                                $rs=$db->exec($sql);
                        //帳號或信箱重複
//                      $count=CSQL($db,"SELECT count(*) FROM consumer c,business b WHERE c.account='".$account."' or c.email='".$email."' or b.account='".$account."' or b.email='".$email."'");
//                      $count=CSQL($db,"SELECT count(*) FROM consumer c,business b WHERE c.account='".$account."' or c.email='".$email."' or b.account='".$account."' ");
                        $count=CSQL($db,"SELECT count(*) FROM consumer c,business b WHERE c.account='".$account."'  or b.account='".$account."' ");
                        if($count>0) dierrmsg3(-1,19,"帳號重複");
//                      if(isset($post['IMEI'])){
//                              $count=CSQL($db,"SELECT count(*) FROM consumer  WHERE IMEI='".$IMEI."' ");
//                              if($count>0) dierrmsg3(-1,18,"手機已註冊");
//                      }
                        $sql="INSERT INTO consumer (account, password, lastname, firstname, birthday, city, address, email, phone, IMEI,disp_name, image) VALUES ('".$account."', '".$password."', '".$lastname."', '".$firstname."', '".$birthday."', '".$city."', '".$address."', '".$email."', '".$phone."', '".$IMEI."', '".$dispname."', 'upload\/system\/image\/person.png' )";
//                      $sql="INSERT INTO consumer (account, password, lastname, firstname, birthday, city, address, email, phone, IMEI,disp_name) VALUES ('".$account."', '".$password."', '".$lastname."', '".$firstname."', '".$birthday."', '".$city."', '".$address."', '".$email."', '".$phone."', '".$IMEI."', '".$dispname."')";
                        $rs=$db->exec($sql);
//                      if($rs>0)    // dierrmsg(0,"註冊成功");
                                dierr(0);
//                              dierrmsg3(0,0,"註冊成功");
//                      else dierrmsg(-1,"資料庫忙碌請重試");
                }
                else{
//                              dierr(0);
                        dierrmsg3(-1,6,"軟體錯誤請用正版"); //"資料不可空白"); //手機APP已檢查,若有此現象表示不是用手機APP//資料未填寫完整
                }
        break;

        /*
        功能:商家註冊
        */
        case "business_register":
                if(isset($post['account'])&&isset($post['password'])&&isset($post['business_name'])&&isset($post['birthday'])&&isset($post['city'])&&isset($post['address'])&&isset($post['email'])){
                        if(strlen($post['account'])<6) dierrmsg(-1,"帳號短於6字或大於16字"); //帳號太短
                        if(strlen($post['password'])<6 || strlen($post['password'])>16) dierrmsg(-1,"密碼短於6字或大於16字"); //密碼格式錯誤
                        if(strlen($post['business_name'])<6 ) dierrmsg(-1,"店名太短"); //店名格式錯誤
                        if(!is_DateISO($post['birthday'])) dierrmsg(-1,"日期格式錯誤"); //日期格式錯誤
                        if(!is_city($post['city'])) dierrmsg(-1,"不正確縣市"); //不正確縣市
//                      if(!is_address($post['city'].$post['address'])) dierrmsg(-1,"地址格式:台南市北區公園路"); //不正確地址
                        if(!is_email($post['email'])) dierrmsg(-1,"信箱格式錯誤"); //信箱格式錯誤
// 未來APP才會用到的欄位
//                      if(!isset($post['name'])) dierrmsg(-1,"請給此手機命名"); //未給手機命名
//                      if(strlen($post['IMEI'])!=15) dierrmsg(-1,"請用手機註冊"); //未用手機註冊
//                      if(!is_phone($post['phone'])) dierrmsg(-1,"電話格式:區碼-電話號碼"); //電話格式錯誤
//                      $name=$post['name'];    //手機名稱
//                      $IMEI=GetPassword($post['IMEI']);
//                      $phone=$post['phone'];
                        $phone="OLDBUNOPHONE";
                        $IMEI="OLDBUNOIMEI";
                        $account=$post['account'];
                        $password=GetPassword($post['password']);
                        $business_name=$post['business_name'];          //店家名稱
                        $birthday=$post['birthday'];    //周年慶
                        $city=$post['city'];
                        $address1=get_address($post['address']);
                                $lat=$address1['lat'];
                                $lng=$address1['lng'];
                        $address=$post['address'];
                        $email=$post['email'];
                        if(isset($post['lat'])&&isset($post['lng'])){
                                $lat=$post['lat'];
                                $lng=$post['lng'];
                        }
                                $sql="INSERT INTO regist_detail (account, password, birthday, lastname, city, address, phone, email) VALUES ('".$account."', '".$password."',  '".$birthday."', '".$business_name."', '".$city."', '".$address."', '".$phone."', '".$email."' )";
                                $rs=$db->exec($sql);
//店家可註冊多支手機
                                $num_IMEI=ASQL($db,"SELECT num_IMEI FROM business WHERE account='".$account."' and password='".$password."'  ");
                        if($num_IMEI['num_IMEI'] > 1){   //可註冊2隻以上
//老新店家檢查手機可註冊限額
                                $num_IMEI=ASQL($db,"SELECT num_IMEI FROM business WHERE account='".$account."' and password='".$password."' and business_name='".$business_name."' and email='".$email."' ");
                                if($num_IMEI['num_IMEI'] < 1) dierrmsg(-1,"超出手機可註冊限額");
                                $B_ID=ASQL($db,"SELECT ID FROM business WHERE account='".$account."'");
                                $count=CSQL($db,"SELECT count(*) FROM IMEI WHERE b_ID='".$B_ID['ID']."'  "); //此店家已註冊手機
                                if ( $num_IMEI <= $count) dierrmsg(-1,"超出手機可註冊限額");  //超出手機可註冊限額
                                else {
                                        $count=CSQL($db,"SELECT count(*) FROM IMEI WHERE IMEI='".$IMEI."' or name='".$name."' ");
                                        if (  $count > 0) dierrmsg(-1,"手機已註冊或手機名稱相同");  //手機已註冊
                                        $B_ID=CSQL($db,"SELECT ID FROM business WHERE account='".$account."'");
                                        $db->exec("INSERT INTO IMEI ( b_ID, IMEI, name) VALUES ( '".$B_ID['ID']."', '".$IMEI."', '".$name."'  )");
//                      if($rs > 0)     //dierrmsg(0,"註冊成功");
                                        dierr(0);   //註冊成功
                                }
//                      }
//                      else if($num_IMEI == 1){  //只可註冊1隻
//                              dierrmsg(-1,"已超出手機可註冊限額");  //超出手機可註冊限額
                        } else {    //因預設值為1其他情狀即是新店註冊
//新店家檢查帳號或信箱重複
//                      $count=CSQL($db,"SELECT count(*) FROM consumer c,business b WHERE c.account='".$account."' or c.email='".$email."' or b.account='".$account."' or b.email='".$email."'");
                                $count=CSQL($db,"SELECT count(*) FROM consumer c,business b WHERE c.account='".$account."' or b.account='".$account."' ");
                                if($count>0) dierrmsg(-1,"帳號或信箱重複");
                                $sql="INSERT INTO business (account, password, business_name, birthday, city, address, phone, email, lat, lng, image) VALUES ('".$account."', '".$password."', '".$business_name."', '".$birthday."', '".$city."', '".$address."', '".$phone."', '".$email."', '".$lat."', '".$lng."', 'upload\/system\/image\/comp.png' )";
//                              $sql="INSERT INTO business (account, password, business_name, birthday, city, address, phone, email, lat, lng) VALUES ('".$account."', '".$password."', '".$business_name."', '".$birthday."', '".$city."', '".$address."', '".$phone."', '".$email."', '".$lat."', '".$lng."' )";
                                $rs=$db->exec($sql);
                        }
                        $B_ID=CSQL($db,"SELECT ID FROM business WHERE account='".$account."'");
                        $db->exec("INSERT INTO IMEI ( b_ID, IMEI, name) VALUES ( '".$B_ID['ID']."', '".$IMEI."', '".$name."'  )");
//                      if($rs > 0)     //dierrmsg(0,"註冊成功");
                                dierr(0);   //註冊成功
//                              dierrmsg3(0,0,"註冊成功");
//                      else dierrmsg(-1, "資料庫忙碌請重試");
                }
                else{
                        dierrmsg(-1,"軟體錯誤請用正版"); //"資料不可空白"); //手機APP已檢查,若有此現象表示不是用手機APP//資料未填寫完整
                }
        break;

        /*
        功能:會員登入
        */
        case "login":
//      case "business_login":
//              if(isset($post['identity'])&&isset($post['account'])&&isset($post['password'])&&isset($post['IMEI'])){
                if(isset($post['identity'])&&isset($post['account'])&&isset($post['password'])){
                        $account=$post['account'];
                        $password=GetPassword($post['password']); //加以編碼
                        $identity=$post['identity'];
                        if(! isset($post['IMEI'])){
                                $IMEI="OLD NO IMEI";
                        } else {
                                $IMEI=GetPassword($post['IMEI']);
                        }
                        if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && isset($_SERVER['HTTP_VIA'])){
                                $remote_ip =$_SERVER['HTTP_X_FORWARDED_FOR'];
                        }else{
                                $remote_ip =$_SERVER['REMOTE_ADDR'];
                        }
                        if($identity=="consumer" ) $i_ID=1;
                        elseif ($identity=="business" )  $i_ID=2;
                        elseif ($identity!="appliance")  $i_ID=3;//身分ID
                        else  dierrmsg3(-1, 6,"軟體錯誤請用正版"); //身分錯誤  $i_ID=0;   //身分錯誤以0表示
                        $browser=$_SERVER['HTTP_USER_AGENT'];
//核對侵入密碼
                        $count=CSQL($db,"SELECT count(*) FROM login_detail WHERE account='".$account."' and password='".$password."' ");
                        if($count < 1){
//防止侵入,沒有的都要記錄
                                $sql="INSERT INTO login_detail (identity, account, password,  inoutsta, ip, browser,IMEI) VALUES ('".$i_ID."', '".$account."', '".$password."', '1', '".$remote_ip."', '".$browser."', '".$IMEI."')";
//                              $sql="INSERT INTO login_detail (identity, account, password,  inoutsta, ip) VALUES ('".$i_ID."', '".$account."', '".$password."', '1', '".$remote_ip."')";
//                              $sql="INSERT INTO login_detail (identity, account, password, time, inoutsta) VALUES ('".$identity."', '".$account."', '".$password."', now(), 'login')";
                                $rs=$db->exec($sql);
                        } else {
                                $count=CSQL($db,"SELECT count(*) FROM login_detail WHERE account='".$account."' and password='".$password."' and time > date_add(now(),interval -30 minute)");
//                              $count=CSQL($db,"SELECT count(*) FROM login_detail WHERE account='".$account."' and password='".$password."' and time > date_add(now(),interval -3 hour)");
                                if($count < 1){
//防止侵入,超過30分太久沒登入的都要記錄
                                        $sql="INSERT INTO login_detail (identity, account, password,  inoutsta, ip, browser, IMEI) VALUES ('".$i_ID."', '".$account."', '".$password."', '1', '".$remote_ip."', '".$browser."', '".$IMEI."')";
//                                      $sql="INSERT INTO login_detail (identity, account, password,  inoutsta, ip) VALUES ('".$i_ID."', '".$account."', '".$password."', '1', '".$remote_ip."')";
                                        $rs=$db->exec($sql);
                                }
                        }
//有紀錄的查時間是否超過30分
//                      if($count < 1){
//防止侵入,超過30分的都要記錄
//                              $sql="INSERT INTO login_detail (identity, account, password, time, inoutsta) VALUES ('".$identity."', '".$account."', '".$password."', now(), 'login')";
//                              $rs=$db->exec($sql);
//                      }
//真正開始登入程序
                        if(strlen($account)<2 || strlen($account)>16) dierrmsg(-1, "帳號格式錯誤"); //帳號格式錯誤
                        if(strlen($post['password'])<2 || strlen($post['password'])>16) dierrmsg(-1, "密碼格式錯誤"); //密碼格式錯誤
                        if($identity!="consumer" && $identity!="business" && $identity!="appliance") dierrmsg(-1, "軟體錯誤請用正版"); //身分錯誤
//檢查帳號
                        $count=CSQL($db,"SELECT count(*) FROM ".$identity." WHERE account='".$account."'");
                        if($count>0){
//真正核對密碼,login_detail對了,可能是再次入侵
                        $count=CSQL($db,"SELECT count(*) FROM ".$identity." WHERE account='".$account."' and password='".$password."'");
                                if($count==1){
//                                      if($identity=="consumer"){
//                                              $rs=$db->exec("UPDATE  consumer SET  online =  '1', time =  now() WHERE account='".$account."' and password='".$password."'");
//                                      }
                                        $row = ASQL($db,"SELECT * FROM  ".$identity." WHERE account='".$account."' and password='".$password."'");
                                        $array[]=$row;
//                                      $_SESSION['new']="";   //不可清除,因舊版一直在login
                                        $_SESSION['first']=1;   //login 配合receive_message
                                        $_SESSION['identity']=$identity;
                                        $_SESSION['account']=$account;
                                        $_SESSION['account_ID']=$row['ID'];    //帳戶ID
                                        $_SESSION['IMEI']=$IMEI;  //有註冊IMEI
                                        if($identity=="consumer"){
                                                $rs=SQL($db,"SELECT * FROM instant_message WHERE  receive_account='".$account."' and received=0  ");
                                                while($row = $rs->fetch(PDO::FETCH_ASSOC)){
                                                        $pos =strpos($_SESSION['new'], $row['send_account'].":");
                                                        if ( $pos === false){
                                                                $_SESSION['new']=$_SESSION['new'].$row['send_account'].": ";
//                                                      if ( strpos($_SESSION['new'], $row['send_account'].":")){
//                                                      }else{
                                                        }
                                                }
                                                if( $IMEI != $row['IMEI']){
//                                                              session_destroy();
//                                                              dierrmsg3(-1, 7, "請用原註冊手機");//登入要IMEI,沒註冊IMEI錯誤
                                                }
                                                $rs=$db->exec("UPDATE consumer SET online='1', time = now() WHERE account='".$account."' and password='".$password."'");
                                        }
                                        elseif( $identity=="business"){
                                                        $IMEI_ID= CSQL($db,"SELECT ID FROM  IMEI WHERE IMEI='".$IMEI."' and b_ID='".$row['ID']."' ");
                                                        if( $IMEI_ID['ID'] > 0)
                                                                $_SESSION['IMEI_ID']=$IMEI_ID['ID'];  //有註冊IMEI
                                                        else {
//加入清除所有$_SESSION[]變數
//                                                              session_destroy();
//                                                              dierrmsg3(-1, 7, "請用原註冊手機");//店家登入要IMEI,沒註冊IMEI錯誤
                                                        }
                                        }
                                        $arr[]=array("msg"=>0);
//                                      $arr[]=array("msg"=>0,"message"=>$_SESSION['new']);
                                        $array=array_merge($arr,$array);
                                        echo json_encode($array);
                                }
                                else dierrmsg(-1,"帳號或密碼錯誤"); //帳號或密碼錯誤
                        }
                        else dierrmsg(-1,"帳號或密碼錯誤"); //查無此帳號
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //資料未填寫完整
                }
        break;

        /*
        功能:登入
        */
        case "consumer_login":
//      case "login":
                if(isset($post['identity'])&&isset($post['account'])&&isset($post['password'])&&isset($post['IMEI'])){
                        $account=$post['account'];
                        $password=GetPassword($post['password']); //加以編碼
                        $IMEI=GetPassword($post['IMEI']); //加以編碼
                        $identity=$post['identity'];
                        if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && isset($_SERVER['HTTP_VIA'])){
                                $remote_ip =$_SERVER['HTTP_X_FORWARDED_FOR'];
                        }else{
                                $remote_ip =$_SERVER['REMOTE_ADDR'];
                        }
                        if($identity == "consumer" ) $i_ID=1;
                        elseif ($identity == "business" )  $i_ID=2;
                        elseif ($identity == "appliance")  $i_ID=3;   //身分ID
                        else  dierrmsg3(-1, 6,"軟體錯誤請用正版"); //身分錯誤  $i_ID=0;   //身分錯誤以0表示
                        $browser=$_SERVER['HTTP_USER_AGENT'];
//核對侵入帳號密碼
//                      $rs=CSQL($db,"SELECT count(*) FROM login_detail WHERE account='".$account."' and password='".$password."' and time > date_add(now(),interval -30 minute)");
//防止侵入,所有登入都要記錄
//                      if($rs < 1){
//防止侵入,登入沒有的都要記錄
                                $sql="INSERT INTO login_detail (identity, account, password,  inoutsta, ip, browser) VALUES ('".$i_ID."', '".$account."', '".$password."', '1', '".$remote_ip."', '".$browser."')";
//                              $sql="INSERT INTO login_detail (identity, account, password, time, inoutsta) VALUES ('".$identity."', '".$account."', '".$password."', now(), 'login')";
                                $rs=$db->exec($sql);
//                      } else {
//                              $count=CSQL($db,"SELECT count(*) FROM login_detail WHERE account='".$account."' and password='".$password."' and time > date_add(now(),interval -30 minute)");
//                              $count=CSQL($db,"SELECT count(*) FROM login_detail WHERE account='".$account."' and password='".$password."' and time > date_add(now(),interval -3 hour)");
//                              if($count < 1){
//防止侵入,有的超過30分太久沒登入的都要記錄
//                                      $sql="INSERT INTO login_detail (identity, account, password,  inoutsta, ip, browser) VALUES ('".$i_ID."', '".$account."', '".$password."', '1', '".$remote_ip."', '".$browser."')";
//                                      $sql="INSERT INTO login_detail (identity, account, password,  inoutsta, ip) VALUES ('".$i_ID."', '".$account."', '".$password."', '1', '".$remote_ip."')";
//                                      $rs=$db->exec($sql);
//                              }
//                      }
//真正開始登入程序
                        if(strlen($account)<5 || strlen($account)>16) dierrmsg3(-1, 4,"帳號必須6字以上"); //帳號格式錯誤
                        if(strlen($post['password'])<5 || strlen($post['password'])>16) dierrmsg3(-1, 5,"密碼必須6字以上"); //密碼格式錯誤
                        if($identity!="consumer" && $identity!="business" && $identity!="appliance") dierrmsg3(-1, 6,"軟體錯誤請用正版"); //身分錯誤
//                      if(!($identity=="consumer" || $identity=="business" || $identity=="appliance")){
//                                              dierrmsg3(-1, 6,"軟體錯誤請用正版"); //身分錯誤
//                      }
//檢查帳號
//                      $count=CSQL($db,"SELECT count(*) FROM ".$identity." WHERE account='".$account."'");
//                      if($count>0){
//真正核對密碼,login_detail對了,可能是再次入侵
//                      $count=CSQL($db,"SELECT count(*) FROM ".$identity." WHERE account='".$account."' and password='".$password."'");
//                      if($count==1){     //核對帳號密碼正確
                                $row = ASQL($db,"SELECT * FROM  ".$identity." WHERE account='".$account."' and password='".$password."' ");
                                if ($row > 0){
                                        $array[]=$row;  //取得帳戶資料回覆
                                        $_SESSION['first']=1;   //login 配合receive_message
                                        $_SESSION['identity']=$identity;
                                        $_SESSION['account']=$account;
                                                $_SESSION['account_ID']=$row['ID'];    //帳戶ID
                                                $_SESSION['IMEI']=$IMEI;  //有註冊IMEI
//                                      if($identity=="consumer"){
//                                              $rs=$db->exec("UPDATE  consumer SET  online =  '1', time =  now() WHERE account='".$account."' and password='".$password."'");
//                                      }
//                                      $row = ASQL($db,"SELECT * FROM  ".$identity." WHERE account='".$account."' and password='".$password."' ");
//                                      if ($row > 0){
//                                              $array[]=$row;  //取得帳戶資料回覆
//                                      }
                                        if($identity=="consumer"){
                                                if( $IMEI !=$row['IMEI']){
                                                                session_destroy();
                                                                dierrmsg3(-1, 7, "請用原註冊手機");//登入要IMEI,沒註冊IMEI錯誤
                                                }
                                                $rs=$db->exec("UPDATE  consumer SET  online =  '1', time =  now() WHERE account='".$account."' and password='".$password."'");
                                        }
                                        elseif( $_SESSION['identity']=="business"){
                                                        $IMEI_ID= CSQL($db,"SELECT ID FROM  IMEI WHERE IMEI='".GetPassword($post['IMEI'])."' ");
                                                        if( $IMEI_ID > 0)
                                                                $_SESSION['IMEI_ID']=$IMEI_ID;  //有註冊IMEI
                                                        else {
//                                                              $_SESSION['IMEI_ID']=0;
//加入清除所有$_SESSION[]變數
                                                                session_destroy();
                                                                dierrmsg3(-1, 7, "請用原註冊手機");//店家登入要IMEI,沒註冊IMEI錯誤
                                                        }
                                        }
//                                      $arr3[]=array("msg" => 0);           //OK
                                        $arr3[]=array("msg" => 0, "code"=> 0, "message"=>"OK");           //OK
//                                      $arr3[]=array("msg" => 0, "code" => 0, "message" => "OK");  //新APP OK,IEok但APP不行
                                        $array=array_merge($arr3,$array);
                                        echo json_encode($array);
                                }
                                else dierrmsg3(-1,2,"帳號或密碼錯誤"); //帳號或密碼錯誤
//                      }
//                      else dierrmsg3(-1,2,"帳號或密碼錯誤"); //查無此帳號
                }
                else{
                        dierrmsg3(-1,3,"資料不可空白"); //資料未填寫完整
                }
        break;

        /////*  功能:取得商家粉絲
        case "business_friend":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
//              if(isset($post['password'])){
                        if($_SESSION['identity']=='business'){
//                              $identity=$_SESSION['identity'];
                                $account=$_SESSION['account'];
//                              $password=GetPassword($post['password']);
//核對登入記錄(侵入密碼)
//                              $rs=CSQL($db,"SELECT count(*) FROM login_detail WHERE account='".$account."' and password='".$password."' and time > date_add(now(),interval -30 minute)");
//                              if($rs < 1){   //沒有登入記錄
//防止侵入,登入記錄沒有的都要記錄
//                                      $sql="INSERT INTO login_detail (identity, account, password, time, inoutsta) VALUES ('".$identity."', '".$account."', '".$password."', now(), 'business_friend')";
//                                      $rs1=$db->exec($sql);
//                              }
//                                      dierrmsg(-1,"登入記錄檢查完"); //登入記錄檢查完
//真正核對密碼,login_detail對了,可能是再次入侵,還要核對帳戶資料
//                              $count=CSQL($db,"SELECT count(*) FROM ".$identity." WHERE account='".$account."' and password='".$password."'");
//                              $count=CSQL($db,"SELECT count(*) FROM business WHERE account='".$account."' and password='".$password."'");
//                                      dierrmsg(-1,"密碼檢查完"); //密碼檢查完
//                              if($count==1){
//                                      dierrmsg(-1,"密碼檢查OK"); //密碼檢查OK
//original 只在consumer_record查朋友資料
                                        $rs=SQL($db,"SELECT distinct(c_ID),c1.email,b1.business_name FROM `consumer_record` r1,`consumer` c1,`business` b1 WHERE b1.account='".$account."' and r1.b_ID=b1.ID and c_ID=c1.ID");
//同時在consumer_record exchange_record查朋友資料
//                                      $rs=SQL($db,"SELECT distinct(c_ID),c1.email,b1.business_name FROM `consumer_record` r1, `exchange_record` r2,`consumer` c1,`business` b1 WHERE (b1.account='".$account."' and r1.b_ID=b1.ID and c_ID=c1.ID )or (b1.account='".$account."' and r2.b_ID=b1.ID and c_ID=c1.ID )");
//                                      $rs=SQL($db,"SELECT distinct(c_ID),c_ID,c1.email,b1.business_name FROM `consumer_record` r1, `exchange_record` r2,`consumer` c1,`business` b1 WHERE b1.account='".$account."' and r1.b_ID=b1.ID or r2.b_ID=b1.ID and c_ID=c1.ID ");
                                        while($row=$rs->fetch(PDO::FETCH_ASSOC)){
                                                $cid_array[]=$row['c_ID'];
                                                $friend=ASQL($db,"SELECT account,online,lastname,firstname FROM consumer WHERE ID='".$row['c_ID']."'");
                                                $array[]=array("account"=>$friend['account'],"online"=>$friend['online'],"lastname"=>$friend['lastname'],"firstname"=>$friend['firstname']);
                                        }
                                        if(!isset($array)) $array=array(); //consumer_record查無朋友資料//exchange_record查無朋友資料
//只在exchange_record查朋友資料
                                        $rs=SQL($db,"SELECT distinct(c_ID),c1.email,b1.business_name FROM `exchange_record` r1,`consumer` c1,`business` b1 WHERE b1.account='".$account."' and r1.b_ID=b1.ID and c_ID=c1.ID");
                                        while($row=$rs->fetch(PDO::FETCH_ASSOC)){
                                                if ( array_search($row['c_ID'], $cid_array )<0 ){
                                                        $friend=ASQL($db,"SELECT account,online,lastname,firstname FROM consumer WHERE ID='".$row['c_ID']."'");
                                                        $array1[]=array("account"=>$friend['account'],"online"=>$friend['online'],"lastnamet"=>$friend['lastname'],"firstname"=>$friend['firstname']);
                                                }
                                        }
                                        if(!isset($array1)) $array1=array(); //exchange_record查無朋友資料
                                        $arr[]=array("msg"=>0);
//                                      $array=array_merge($arr,$array);
                                        $array=array_merge($arr,$array,$array1);
                                        echo json_encode($array);
//                              }
//                              else
//                                      dierrmsg(-1,"密碼錯誤"); //密碼錯誤
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
//              }
//              else{
//                      dierrmsg(-1,"資料不可空白"); //未登入or資料未填寫完整
//              }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入or資料未填寫完整
                }
        break;

        ////////////*   功能:商家優惠訊息修改
        case "business_edit":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['deals'])){
                        if($_SESSION['identity']=='business'){
                                $account=$_SESSION['account'];
                                $deals=$post['deals'];
//                              $rs=SQL($db,"SELECT distinct(c_ID), c1.email, b1.business_name, b1.address, bi.city FROM consumer_record as r1 INNER JOIN consumer as c1 INNER JOIN business as b1 WHERE b1.account='".$account."' and r1.b_ID=b1.ID and c_ID=c1.ID");
//                              $rs=SQL($db,"SELECT distinct(c_ID), c1.email, b1.business_name, b1.address, bi.city FROM `consumer_record` r1,`consumer` c1,`business` b1 WHERE b1.account='".$account."' and r1.b_ID=b1.ID and c_ID=c1.ID");
                                $rs=SQL($db,"SELECT distinct(c_ID),c1.email,b1.business_name  FROM `consumer_record` r1,`consumer` c1,`business` b1 WHERE b1.account='".$account."' and r1.b_ID=b1.ID and c_ID=c1.ID");
//會衝到email                           $rs=SQL($db,"SELECT distinct(c_ID),c1.email,b1.business_name,b1.email FROM `consumer_record` r1,`consumer` c1,`business` b1 WHERE b1.account='".$account."' and r1.b_ID=b1.ID and c_ID=c1.ID");
//                              dierrmsg(0,"優惠訊息修到此一遊"); //測試訊息
                                if($rs>0) {
                                        while($row=$rs->fetch(PDO::FETCH_ASSOC)){
                                                sendMail2($row['email'],$row['business_name']."優惠訊息-".$deals,"<h1>您好:<br>歡迎使用金銀島</h1><a href=http://ksu.estar.com.tw:8080/~water/>雲端購物集點系統</a><br><h2>".$row['business_name'].":</h2><br><h1>".$deals."</h1>");
//                                              sendMail2($row['email'],$row['business_name']."優惠訊息-".$deals,"<h1>你好歡迎使用金銀島</h1><a href=http://ksu.estar.com.tw:8080/~water/>雲端購物集點系統</a><br><h2>".$row['city'].$row['business_name'].":</h2><br>".$row['address']."<br>".$deals);
//                                              sendMail2($row['email'],$row['email'],$row['business_name']."優惠訊息-".$deals,"<h1>你好歡迎使用金銀島</h1><a href=http://ksu.estar.com.tw:8080/~water/>雲端購物集點系統</a><br>".$row['business_name'].$deals);
//                                              sendMail2($row['email'],$row['business_name']."優惠訊息-".$deals,"歡迎使用金銀島\r\n".$deals);
                                        }
//                              dierrmsg(0,"優惠訊息修到此一遊"); //測試訊息
                                        $rs=$db->exec("UPDATE  business SET  deals =  '".$deals."' WHERE account='".$account."'");
                                        dierr(0); //修改成功
                                }
//      rs=0表示"資訊一樣未修改"沒有修改
                                else
                                        dierrmsg(-1,"尚無消費者"); //找不到無消費者
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //未登入or資料未填寫完整
                }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入or資料未填寫完整
                }
        break;

        ///////////////*        功能:商家位置(經緯度)修改
        case "location_edit":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['lat'])&&isset($post['lng'])){
                        if($_SESSION['identity']=='business'){
                                $account=$_SESSION['account'];
                                $lat=$post['lat'];
                                $lng=$post['lng'];
                                $sql="INSERT INTO lnglat_detail (lng, lat, distance, time) VALUES ('".$lng."', '".$lat."', '0', now())";
                                $rs=$db->exec($sql);
                                $rs=$db->exec("UPDATE  business SET lat= '".$lat."', lng= '".$lng."' WHERE account='".$account."'");
//                              if($rs>0) {
//                                      $sql="INSERT INTO lnglat_detail (lng, lat, distance, time) VALUES ('".$lng."', '".$lat."', '1', now())";
//                                      $rs=$db->exec($sql);
                                        dierr(0); //修改成功
//                                      dierrmsg(0,"修改成功"); //修改成功
//                              }
//                              else dierrmsg(-1,"資訊一樣未修改"); //修改失敗
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //未登入or資料未填寫完整
                }
                }
                else{
                        dierrmsg(-1,"未登入"); //未登入or資料未填寫完整
                }
        break;

        //////////////////*     功能:編輯消費者好友
        case "friend_edit":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['action'])&&isset($post['friend_account'])){
                        if($post['action']!="add"&&$post['action']!="delete") dierrmsg(-1,"動作錯誤"); //動作錯誤
                        if($_SESSION['identity']=='consumer'){
                                $account=$_SESSION['account'];
                                $friend_account=$post['friend_account'];
                                $ID=CSQL($db,"SELECT ID FROM consumer WHERE account='".$account."'");
                                $friend_ID=CSQL($db,"SELECT ID FROM consumer WHERE account='".$friend_account."'");
                                if($friend_ID>0){
                                        if($post['action']=="add"){
                                                $count=CSQL($db,"SELECT count(*) FROM friend WHERE C_ID='".$ID."' and F_ID='".$friend_ID."'");
                                                if($count>0) dierrmsg(0,"已加過好友"); //已加過好友
                                                else{
                                                        $rs=$db->exec("INSERT INTO friend(C_ID, F_ID, ack) VALUES('".$ID."', '".$friend_ID."', 1)");
                                                        $rs=$db->exec("INSERT INTO friend(F_ID, C_ID, ack) VALUES('".$ID."','".$friend_ID."', 0)");
                                                }
                                        }
                                        else if($post['action']=="delete"){
//                                              $rs=$db->exec("DELETE FROM friend WHERE C_ID='".$ID."' and F_ID='".$friend_ID."'");
                                                $rs=$db->exec("update friend set del=1 WHERE C_ID='".$ID."' and F_ID='".$friend_ID."'");
                                        }
//                                      if($rs>0) dierrmsg(0,"加好友成功"); //執行成功
                                        dierr(0); //執行成功
                                        //else dierr(-1); //執行失敗
                                }
                                else dierrmsg(-1,"查無此帳號"); //查無此帳號
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //未登入or資料未填寫完整
                }
                }
                else{
                        dierrmsg(-1,"未登入"); //未登入or資料未填寫完整
                }
        break;

        ///////////////*        功能:消費者好友
        case "friend_list":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                        if($_SESSION['identity']=='consumer'){
                                $rs=SQL($db,"SELECT account FROM consumer WHERE time<date_add(now(),interval -30 minute)"); //超過30分鐘未登入,判斷為下線
                                while($row = $rs->fetch(PDO::FETCH_ASSOC)){
                                        $rs1=$db->exec("UPDATE  consumer SET  online =  '0' WHERE account='".$row['account']."'"); //更新為下線狀態
                                }

                                $account=$_SESSION['account'];
                                $ID=CSQL($db,"SELECT ID FROM consumer WHERE account='".$account."'");
//original
                                $rs=SQL($db,"SELECT F_ID FROM friend WHERE C_ID='".$ID."'" );
//                              $rs=SQL($db,"SELECT * FROM friend WHERE C_ID='".$ID."' or F_ID='".$ID."' ");
//                              $rs=SQL($db,"SELECT F_ID FROM friend WHERE C_ID='".$ID."' union SELECT C_ID FROM friend WHERE F_ID='".$ID."'");
                                $newFriend=$_SESSION['new'];
                                while($row = $rs->fetch(PDO::FETCH_ASSOC)){
//                                      $friend=ASQL($db,"SELECT account,online,lastname,firstname FROM consumer WHERE ID='".$row['F_ID']."' or ID='".$row['C_ID']."'");
                                        $friend=ASQL($db,"SELECT account,online,lastname,firstname FROM consumer WHERE ID='".$row['F_ID']."' or ID='".$row['C_ID']."'");
                                        $pos =strpos($newFriend, $friend['account']);
                                        if ( $pos === false){
//                                      if ( strpos($newFriend, $friend['account'].":")){
                                                $array[]=array("account"=>$friend['account'],"online"=>"0");
//                                              $array[]=array("account"=>$friend['account'],"online"=>"0","new"=>$newFriend);
                                        }
                                        else{
                                                $array[]=array("account"=>$friend['account'],"online"=>"1");
//                                              $array[]=array("account"=>$friend['account'],"online"=>"1","new"=>$newFriend);
                                        }
//                                      $array[]=array("account"=>$friend['account'],"online"=>$friend['online']);
//加入會造成APP不正常
//                                      $array[]=array("account"=>$friend['account'],"online"=>$friend['online'],"lastname"=>$friend['lastname'],"firstname"=>$friend['firstname']);
                                }
                                if(!isset($array)) $array=array(); //查無資料
                                $arr[]=array("msg"=>0);
                                $array=array_merge($arr,$array);
                                echo json_encode($array);
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"未登入"); //未登入
                }
        break;

        ////////////////*       功能:消費者消費紀錄(總)
        case "consumer_record":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                        if($_SESSION['identity']=='consumer'){
                                $account=$_SESSION['account'];
                                $id=CSQL($db,"SELECT ID FROM consumer WHERE account='".$account."'");
                                $recode=SQL($db,"SELECT * FROM consumer_record WHERE c_ID='".$id."' order by time desc");
                                while($row = $recode->fetch(PDO::FETCH_ASSOC)){
                                        $price=0;
                                        $detail=SQL($db,"SELECT * FROM record_detail WHERE SID='".$row['SID']."'");
                                        while($row1 = $detail->fetch(PDO::FETCH_ASSOC)){
                                                $price+=CSQL($db,"SELECT price*".$row1['count']." FROM commodity WHERE ID=".$row1['com_ID']."");
                                        }
                                        $business_name=CSQL($db,"SELECT business_name FROM business WHERE ID=".$row['b_ID']."");
                                        $array[]=array("business_id"=>$row['b_ID'],"business"=>$business_name,"price"=>"$price","time"=>$row['time']);
                                }
                                if(!isset($array)) $array=array(); //查無消費紀錄
                                $arr[]=array("msg"=>0);
                                $array=array_merge($arr,$array);
                                echo json_encode($array);
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        ////////////////*       功能:消費者詳細消費紀錄
        case "record_detail":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['date'])){
                        if($_SESSION['identity']=='consumer'){
                                $account=$_SESSION['account'];
                                $date=$post['date']."-01";
                                $id=CSQL($db,"SELECT ID FROM consumer WHERE account='".$account."'");
                                $recode=SQL($db,"SELECT * FROM consumer_record WHERE c_ID='".$id."' and time>'".$date."' and time<date_add('".$date."',interval +1 year) order by time desc");
                                while($row = $recode->fetch(PDO::FETCH_ASSOC)){
                                        $price=0;
                                        $detail=SQL($db,"SELECT * FROM record_detail WHERE SID='".$row['SID']."'");
                                        while($row1 = $detail->fetch(PDO::FETCH_ASSOC)){
                                                $price+=CSQL($db,"SELECT price*".$row1['count']." FROM commodity WHERE ID=".$row1['com_ID']."");
                                        }
                                        $business_name=CSQL($db,"SELECT business_name FROM business WHERE ID=".$row['b_ID']."");
                                        $array[]=array("business_id"=>$row['b_ID'],"business"=>$business_name,"price"=>"$price","time"=>$row['time']);
                                }
                                if(!isset($array)) $array=array(); //查無消費紀錄
                                $arr[]=array("msg"=>0);
                                $array=array_merge($arr,$array);
                                echo json_encode($array);
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //未登入or資料未填寫完整
                }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        ///////////////////     功能:消費者消費紀錄(年)
        case "consumer_record_year":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['date'])){
                        if($_SESSION['identity']=='consumer'){
                                $account=$_SESSION['account'];
                                $date=$post['date']."-01";
                                $id=CSQL($db,"SELECT ID FROM consumer WHERE account='".$account."'");
                                $recode=SQL($db,"SELECT * FROM consumer_record WHERE c_ID='".$id."' and time>'".$date."' and time<date_add('".$date."',interval +1 year) order by time desc");
                                while($row = $recode->fetch(PDO::FETCH_ASSOC)){
                                        $price=0;
                                        $detail=SQL($db,"SELECT * FROM record_detail WHERE SID='".$row['SID']."'");
                                        while($row1 = $detail->fetch(PDO::FETCH_ASSOC)){
                                                $price+=CSQL($db,"SELECT price*".$row1['count']." FROM commodity WHERE ID=".$row1['com_ID']."");
                                        }
                                        $business_name=CSQL($db,"SELECT business_name FROM business WHERE ID=".$row['b_ID']."");
                                        $array[]=array("business_id"=>$row['b_ID'],"business"=>$business_name,"price"=>"$price","time"=>$row['time']);
                                }
                                if(!isset($array)) $array=array(); //查無消費紀錄
                                $arr[]=array("msg"=>0);
                                $array=array_merge($arr,$array);
                                echo json_encode($array);
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //未登入or資料未填寫完整
                }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        ////////////    功能:消費者消費紀錄(月)
        case "consumer_record_month":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['date'])){
                        if($_SESSION['identity']=='consumer'){
                                $account=$_SESSION['account'];
                                $date=$post['date']."-01";
                                $id=CSQL($db,"SELECT ID FROM consumer WHERE account='".$account."'");
                                $recode=SQL($db,"SELECT * FROM consumer_record WHERE c_ID='".$id."' and time>'".$date."' and time<date_add('".$date."',interval +1 month) order by time desc");
                                while($row = $recode->fetch(PDO::FETCH_ASSOC)){
                                        $price=0;
                                        $detail=SQL($db,"SELECT * FROM record_detail WHERE SID='".$row['SID']."'");
                                        while($row1 = $detail->fetch(PDO::FETCH_ASSOC)){
                                                $price+=CSQL($db,"SELECT price*".$row1['count']." FROM commodity WHERE ID=".$row1['com_ID']."");
                                        }
                                        $business_name=CSQL($db,"SELECT business_name FROM business WHERE ID=".$row['b_ID']."");
                                        $array[]=array("business_id"=>$row['b_ID'],"business"=>$business_name,"price"=>"$price","time"=>$row['time']);
                                }
                                if(!isset($array)) $array=array(); //查無消費紀錄
                                $arr[]=array("msg"=>0);
                                $array=array_merge($arr,$array);
                                echo json_encode($array);
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //未登入or資料未填寫完整
                }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        ///////////////////*功能:消費者消費紀錄(日)
        case "consumer_record_day":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if($post['date']){
                        if($_SESSION['identity']=='consumer'){
                                $account=$_SESSION['account'];
                                $date=$post['date'];
                                $id=CSQL($db,"SELECT ID FROM consumer WHERE account='".$account."'");
                                $recode=SQL($db,"SELECT * FROM consumer_record WHERE c_ID='".$id."' and time>'".$date."' and time<date_add('".$date."',interval +1 day) order by time desc");
                                while($row = $recode->fetch(PDO::FETCH_ASSOC)){
                                        $price=0;
                                        $detail=SQL($db,"SELECT * FROM record_detail WHERE SID='".$row['SID']."'");
                                        while($row1 = $detail->fetch(PDO::FETCH_ASSOC)){
                                                $price+=CSQL($db,"SELECT price*".$row1['count']." FROM commodity WHERE ID=".$row1['com_ID']."");
                                        }
                                        $business_name=CSQL($db,"SELECT business_name FROM business WHERE ID=".$row['b_ID']."");
                                        $array[]=array("business_id"=>$row['b_ID'],"business"=>$business_name,"price"=>"$price","time"=>$row['time']);
                                }
                                if(!isset($array)) $array=array(); //查無消費紀錄
                                $arr[]=array("msg"=>0);
                                $array=array_merge($arr,$array);
                                echo json_encode($array);
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //未登入or資料未填寫完整
                }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        ////////////////*       功能:搜尋商家位置(地圖導航)
        case "search_business":
                if(isset($post['city'])&&isset($post['address'])){
                        if(!is_address($post['city'])) dierrmsg(-1,"縣市格式錯誤"); //縣市格式錯誤
                        if(!is_address($post['address'])) dierrmsg(-1,"地址格式錯誤"); //地址格式錯誤
                        $city=$post['city'];
                        $address=$post['address'];
                        $rs=SQL($db,"SELECT address FROM business WHERE city LIKE '%".$city."%' AND address LIKE '%".$address."%'");
                        while($row = $rs->fetch(PDO::FETCH_ASSOC)){
                                $array[]=$row;
                        }
                        if(!isset($array)) $array=array(); //查無商家資料
                        $arr[]=array("msg"=>0);
                        $array=array_merge($arr,$array);
                        echo json_encode($array);
                }
                else{
                        dierrmsg(-1,"未填寫縣市"); //資料未填寫完整
                }
        break;

        /////////////////*      功能:搜尋附近所有商家(經緯度)
        case "search_nearby":
                if(isset($post['lat'])&&isset($post['lng'])&&isset($post['distance'])){
                        if(!is_numeric($post['lat'])||!is_numeric($post['lng'])||!is_numeric($post['distance'])) dierrmsg(-1,"格式錯誤"); //格式錯誤
                        $lat=$post['lat'];
                        $lng=$post['lng'];
                        $distance=$post['distance'];
                        $account=$_SESSION['account'];
                        $accountID=$_SESSION['account_ID'];
                        $sql="INSERT INTO lnglat_detail (lng, lat, distance, time, C_ID) VALUES ('".$lng."', '".$lat."', '".$distance."', now(), '".$accountID."')";
                        $rs=$db->exec($sql);
//orginal
//                      $rs=SQL($db,"SELECT ID ,business_name ,lat ,lng , ( 3959 * acos( cos( radians(".$lat.") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(".$lng.") ) + sin( radians(".$lat.") ) * sin( radians( lat ) ) ) ) AS distance FROM business HAVING distance < ".$distance."/20 ORDER BY distance");
//                      $rs=SQL($db,"SELECT ID ,business_name ,lat ,lng , ( 3959 * acos( cos( radians(".$lat.") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(".$lng.") ) + sin( radians(".$lat.") ) * sin( radians( lat ) ) ) ) AS distance FROM business HAVING distance < ".$distance." ORDER BY distance");
                        $rs=SQL($db,"SELECT ID ,business_name ,lat ,lng , ( 3959 * acos( cos( radians(".$lat.") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(".$lng.") ) + sin( radians(".$lat.") ) * sin( radians( lat ) ) ) ) AS distance FROM business HAVING distance < 300 ORDER BY distance");
                        while($row = $rs->fetch(PDO::FETCH_ASSOC)){
                                $array[]=$row;
//hwang
//                              $sql="INSERT INTO lnglat_detail (lng, lat, distance, time) VALUES (".$row['lng'].", ".$row['lat'].", 1, now())";
//                              $rs1=$db->exec($sql);
                        }
                        if(!isset($array)) $array=array(); //查無附近商家
                        $arr[]=array("msg"=>0);
                        $array=array_merge($arr,$array);
                        echo json_encode($array);
                }
                else{
                        dierrmsg(-1,"資料未填寫完整"); //資料未填寫完整
                }
        break;

        //////////////////*     功能:查詢點數
        case "inquiry_point":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                        $identity=$_SESSION['identity'];
                        $account=$_SESSION['account'];
                        $point=CSQL($db,"SELECT ".$identity."_point FROM ".$identity." WHERE account='".$account."'");
                        if(isset($point)) $array[]=array("point"=>$point);
                        else $array[]=array();   //dierrmsg(-1,"查無資料"); //查無資料
                        $arr[]=array("msg"=>0);
                        $array=array_merge($arr,$array);
                        echo json_encode($array);
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        ////////////////////*   功能:贈送朋友紅利點數
        case "send_point":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
//不可送給自己
                if(isset($post['consumer_account'])&&isset($post['point'])&&($_SESSION['account']!=$post['consumer_account'])){
                        if($_SESSION['identity']=='business' || $_SESSION['identity']=='consumer'){
                                $identity=$_SESSION['identity'];
                                //if($identity=='consumer')
//                              $recv_identity="consumer";   //消費者才能接收點數
                                $account=$_SESSION['account'];
                                $point=$post['point'];
                                $consumer_account=$post['consumer_account'];
//發送者不可贈送點數給商家
                                $count=CSQL($db,"SELECT count(*) FROM business WHERE account='".$consumer_account."'");
                                if($count==0){
//檢查接收者
//                                      $count=CSQL($db,"SELECT count(*) FROM consumer WHERE account='".$consumer_account."'");
//                                      if($count==1){
                                        $rs=ASQL($db,"SELECT * FROM consumer WHERE account='".$consumer_account."'");
                                        if($rs>0){
//                                              $row=$rs->fetch(PDO::FETCH_ASSOC);
                                                $rece_ID=$rs['ID'];
//                                              $array[]=$rs;
//                                              $rece_ID=$array['ID'];
                                        }
                                        else dierrmsg(-1,"查無接收者帳號"); //查無接收帳號
//                                      dierrmsg(-1,"$rece_ID");
//檢查發送者
//                                              $check=CSQL($db,"SELECT ".$identity."_point FROM ".$identity." WHERE account='".$account."'");
                                        $rs1=ASQL($db,"SELECT * FROM ".$identity." WHERE account='".$account."'");
                                        if($rs1>0){
//                                              $array[]=$rs1;
//                                              $row=$rs1->fetch(PDO::FETCH_ASSOC);
                                                $send_ID = $rs1['ID'];
                                                if($identity=='consumer')
                                                        $check = $rs1['consumer_point'];
                                                else
                                                        $check = $rs1['business_point'];

//                                      dierrmsg(-1,"$check");
                                                if( $check < $point ) dierrmsg(-1,"餘額點數不足"); //餘額點數不足
//雙方都需要更新點數,放在try內會造成當機,把它移出來試看看
                                                sendMqtt($consumer_account, "point");
                                                sendMqtt($account, "point");
                                                try{
                                                        $db->beginTransaction();
//扣除發送者點數加入接收者
                                                        $transaction=$db->exec("UPDATE ".$identity." t1,consumer t2 set t1.".$identity."_point=t1.".$identity."_point-".$point.",t2.consumer_point=t2.consumer_point+".$point." where t1.account='".$account."' and t2.account='".$consumer_account."'");
//核對用點數轉於記錄
                                                        $transaction1=$db->exec("INSERT INTO transfer_record (c_ID, send_ID, identity, point) values ( '".$rece_ID."', '".$send_ID."', '".$identity."' , '".$point."') ");
                                                        $db->commit();
                                                        dierr(0); //交易完成
//                                                      dierrmsg(0,"交易完成"); //交易完成
                                                }
                                                catch(PDOException $ex){
                                                        $db->rollBack();
                                                        dierrmsg(-1,"資料庫忙碌請重試"); //交易失敗
                                                }
                                        }
                                        else dierrmsg(-1,"軟體錯誤請用正版"); //查無發送者帳號
                                }
                                else dierrmsg(-1,"軟體錯誤請用正版"); //發送者不可贈送點數給商家
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //未登入or資料未填寫完整
                }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        ////////////////////    功能:原新增商品
        case "old_add_commodity":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['name'])&&isset($post['detail'])&&isset($post['price'])&&$_FILES['file']['error']==0){
                        if($_SESSION['identity']=='business'){
                                if(match_strcmp(strtoupper(pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION)),strtoupper("JPG"))||
                                        match_strcmp(strtoupper(pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION)),strtoupper("PNG"))||
                                        match_strcmp(strtoupper(pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION)),strtoupper("BMP"))
                                ){
                                        $account=$_SESSION['account'];
                                        $name=$post['name'];
                                        $detail=$post['detail'];
                                        $price=$post['price'];
                                        $B_ID=CSQL($db,"SELECT ID FROM business WHERE account='".$account."'");
                                        $rs=$db->exec("INSERT INTO commodity(b_ID, name, detail, price) VALUES ('".$B_ID."','".$name."','".$detail."','".$price."')");
                                        $last_id=$db->lastInsertId();
                                        $file_path="upload/".$last_id;
                                        $file_name=$_FILES['file']['name'];
                                        isDirExist($file_path);
                                        move_uploaded_file($_FILES['file']['tmp_name'],iconv("utf-8", "big5",$file_path."/".$file_name));
                                        $rs=$db->exec("UPDATE  commodity SET  image =  '".$file_path."/".$file_name."' WHERE  ID =".$last_id."");
                                        $rs=$db->exec("UPDATE  business SET  commodity_variation = commodity_variation+1  WHERE account ='".$account."'");
                                        dierr(0); //商品新增完成
//                                      dierrmsg(0,"商品新增完成"); //商品新增完成
                                }
                                else dierrmsg(-1,"只接受圖檔上傳"); //只接受圖檔上傳
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //未登入or資料未填寫完整
                }
                }
                else{
                        dierrmsg(-1,"請重新登入");  //未登入or無檔案
                }
        break;

        /////////////////////////       功能:上傳商品圖片
        case "edit_commodity_image":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['commodity_id'])&&$_FILES['file']['error']==0){
                        if($_SESSION['identity']=='business'){
                                if(match_strcmp(strtoupper(pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION)),strtoupper("JPG"))||
                                        match_strcmp(strtoupper(pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION)),strtoupper("PNG"))||
                                        match_strcmp(strtoupper(pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION)),strtoupper("BMP"))
                                ){
                                        $account=$_SESSION['account'];
                                        $commodity_id=$post['commodity_id'];
                                        $path=CSQL($db,"SELECT image FROM commodity WHERE ID='".$commodity_id."'");
                                        $file_path="upload/".$commodity_id;
                                        $file_name=$_FILES['file']['name'];
                                        isDirExist($file_path);
                                        if($path!=$file_path."/".$file_name){
                                                move_uploaded_file($_FILES['file']['tmp_name'],iconv("utf-8", "big5",$file_path."/".$file_name));
                                                $rs=$db->exec("UPDATE  commodity SET  image =  '".$file_path."/".$file_name."' WHERE  ID =".$commodity_id."");
                                                $rs=$db->exec("UPDATE  business SET  commodity_variation = commodity_variation+1  WHERE account ='".$account."'");
                                                if(file_exists(iconv("utf-8", "big5",$path))){
                                                        unlink(iconv("utf-8", "big5",$path));
                                                }
                                        }
                                        dierr(0); //圖片新增完成
//                                      dierrmsg(0,"圖片新增完成"); //圖片新增完成
                                }
                                else dierrmsg(-1,"只接受圖檔上傳"); //只接受圖檔上傳
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //未登入or資料未填寫完整
                }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入or資料未填寫完整
                }
        break;

        ////////////////////////////    功能:新增商品
        case "add_commodity":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['name'])&&isset($post['detail'])&&isset($post['price'])&&isset($post['point'])){
                        if($_SESSION['identity']=='business'){
                                if(isset($post['num_limit'])&&isset($post['person_limit'])){
                                        $num_limit=$post['num_limit'];
                                        $person_limit=$post['person_limit'];
                                }
                                else{
                                        $num_limit=1000;
                                        $person_limit=1;
                                }
                                $account=$_SESSION['account'];
                                $name=$post['name'];
                                $detail=$post['detail'];
                                $price=$post['price'];
                                $point=$post['point'];
                                $B_ID=CSQL($db,"SELECT ID FROM business WHERE account='".$account."'");
//                              $rs=$db->exec("INSERT INTO commodity(b_ID, name, detail, price, point,num_limit,person_limit) VALUES ('".$B_ID."','".$name."','".$detail."','".$price."', '".$point."', '".$num_limit."', '".$person_limit."') ");
                                $rs=$db->exec("INSERT INTO commodity(b_ID, name, detail, price, point,num_limit,person_limit,image) VALUES ('".$B_ID."','".$name."','".$detail."','".$price."', '".$point."', '".$num_limit."', '".$person_limit."', 'upload\/system\/image\/log.jpg' ) ");
                                $rs=$db->exec("UPDATE  business SET  commodity_variation = commodity_variation+1  WHERE account ='".$account."'");
                                dierr(0); //商品新增完成
//                              dierrmsg(0,"商品新增完成"); //商品新增完成
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //資料未填寫完整
                }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入or資料未填寫完整
                }
        break;

        ////////////////////    功能:刪除商品         若已有人購買或兌換則不可刪除
        case "delete_commodity":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['commodity_id'])){
                        if($_SESSION['identity']=='business'){
                                $account=$_SESSION['account'];
                                $commodity_id=$post['commodity_id'];
                                $rs=$db->exec("UPDATE  commodity SET   del_record =  '1' WHERE  ID =".$commodity_id."");
                                $rs=$db->exec("UPDATE  business SET  commodity_variation = commodity_variation+1  WHERE account ='".$account."'");
//                              deleteDirectory("upload/".$commodity_id);
                                dierr(0); //商品刪除成功
//                              dierrmsg(0,"商品刪除成功"); //商品刪除成功
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //資料未填寫完整
                }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        //////////////////*     功能:修改商品
        case "edit_commodity":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['commodity_id'])&&(isset($post['name'])||isset($post['detail'])||isset($post['price'])&&isset($post['point']))){
                        if($_SESSION['identity']=='business'){
                                if(isset($post['name'])&&isset($post['detail'])){
                                        $num_limit=$post['num_limit'];
                                        $person_limit=$post['person_limit'];
                                }
                                else{
                                        $num_limit=100000;   //default limit 100K
                                        $person_limit=0;   //no limit
                                }
                                $account=$_SESSION['account'];
                                $commodity_id=$post['commodity_id'];
//                              if(isset($post['name'])){
                                        $name= $post['name'];
                                        $detail= $post['detail'];
                                        $price= $post['price'];
                                        $point= $post['point'];
                                        $rs=$db->exec("UPDATE  commodity SET  name =  '".$name."', detail =  '".$detail."', price =  '".$price."', point =  '".$point."' WHERE  ID =".$commodity_id."");
//                              }
//                              if(isset($post['detail'])){
//                                      $rs=$db->exec("UPDATE  commodity SET  detail =  '".$detail."' WHERE  ID =".$commodity_id."");
//                              }
//                              if(isset($post['price'])){
//                                      $rs=$db->exec("UPDATE  commodity SET  price =  '".$price."' WHERE  ID =".$commodity_id."");
//                              }
//                              if(isset($post['point'])){
//                                      $rs=$db->exec("UPDATE  commodity SET  point =  '".$point."' WHERE  ID =".$commodity_id."");
//                              }
                                $rs=$db->exec("UPDATE  business SET  commodity_variation = commodity_variation+1  WHERE account ='".$account."'");
                                dierr(0); //商品修改完成
//                              dierrmsg(0,"商品修改完成"); //商品修改完成
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //資料未填寫完整
                }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        /////////////////*      功能:結帳
        case "checkout":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['consumer_account'])&&isset($post['commodity_id'])&&isset($post['IMEI'])){
                        if($_SESSION['identity']=='business'){
                                $account=$_SESSION['account'];
                                $IMEI_ID=$_SESSION['IMEI_ID'];
                                $consumer_account=$post['consumer_account'];
                                $commodity_id=$post['commodity_id'];
                                $IMEI=$post['IMEI'];
//                              $total=$post['total'];
                                $business_id=CSQL($db,"SELECT ID FROM business WHERE account='".$account."'");
//                              $checkIMEI=CSQL($db,"SELECT count(*) FROM consumer WHERE account='".$consumer_account."' and IMEI='".$IMEI."'");
//                              if($checkIMEI==0) dierrmsg(-1,"消費者未使用註冊手機"); //消費者帳號與IMEI碼不同

                                $total_commodity=explode(',',$commodity_id);
                                foreach($total_commodity as $value){
                                        if($value!='') $commodity[]=explode('_',$value);
                                }
                                try{
                                        $db->beginTransaction();
                                        $consumer_ID=CSQL($db,"SELECT ID FROM consumer WHERE account='".$consumer_account."'");
                                        $rs=$db->exec("INSERT INTO consumer_record (c_ID, b_ID,IMEI_ID) VALUES (".$consumer_ID.", ".$business_id.",  ".$IMEI_ID.")");
                                        $last_id=$db->lastInsertId($rs);  //取得comsumer_record的ID
//                                      if($rs==0) throw new PDOException();
                                        $total=0;
                                        foreach($commodity as $k=>$v){
//                                              $check=CSQL($db,"SELECT count(*) FROM commodity WHERE ID=".$v[0]." and off_shelf='0'");
//                                              if($check==0) dierrmsg(-1,"查無此商品"); //查無此商品
//                                              else{
                                                $rsid=ASQL($db,"SELECT * FROM commodity WHERE ID=".$v[0]." and off_shelf='0'");
                                                if($rsid > 0) {
//original
//                                                      $rs=$db->exec("INSERT INTO record_detail (SID, com_ID, count, money) VALUES (".$last_id.",".$v[0].", ".$v[1].")");
//詳細交易記錄
//                                                      $itemtotal= $v[1] * $rsid['price'];
                                                        $itemprice=  $rsid['price'];
                                                        $total=$total + $v[1] * $rsid['price'];
                                                        $rsin=$db->exec("INSERT INTO record_detail (SID, com_ID, count, money) VALUES (".$last_id.",".$v[0].", ".$v[1].", ".$itemprice.")");
                                                        if($rsin==0) throw new PDOException();
                                                } else throw new PDOException(); //查無此商品
                                        }
//交易記錄總價
                                        $rs=$db->exec("UPDATE consumer_record set total='".$total."' where SID= ".$last_id." ");
                                        $db->commit();
                                        dierr(0); //結帳成功
//                                      dierrmsg(0,"結帳成功"); //結帳成功

                                }
                                catch(PDOException $ex){
                                        $db->rollBack();
                                        dierrmsg(-1,"查無此商品請重試"); //結帳失敗
                                }
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //資料未填寫完整
                }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        /////////////////////////////   功能:登出
        case "logout":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
//                      $identity=$_SESSION['identity'];
                        $account=$_SESSION['account'];
                        if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && isset($_SERVER['HTTP_VIA'])){
                                $remote_ip =$_SERVER['HTTP_X_FORWARDED_FOR'];
                        }else{
                                $remote_ip =$_SERVER['REMOTE_ADDR'];
                        }
                        if($identity=="consumer" ) $i_ID=1;
                        elseif ($identity=="business" )  $i_ID=2;
                        elseif ($identity!="appliance")  $i_ID=3;//身分ID
                        else  dierrmsg3(-1, 6,"軟體錯誤請用正版"); //身分錯誤  $i_ID=0;   //身分錯誤以0表示
                        $browser=$_SERVER['HTTP_USER_AGENT'];
                        $rs=$db->exec("UPDATE  consumer SET  online =  '0' WHERE account='".$account."'"); //更新為下線狀態
//                              $sql="INSERT INTO login_detail (identity, account,  time, inoutsta) VALUES ('".$identity."', '".$account."',  now(), 'logout')";
//                              $sql="INSERT INTO login_detail (identity, account,  time, inoutsta) VALUES ('identity', '".$account."',  now(), 'logout')";
                                $sql="INSERT INTO login_detail (identity, account,  inoutsta, ip, browser) VALUES ('".$i_ID."', '".$account."',  '0', '".$remote_ip."', '".$browser."')";
                                $rs=$db->exec($sql);
                        session_destroy();
//                      dierr(0); //登出成功
                        dierrmsg3(0,0,"登出成功"); //登出成功
                }
                else
                        dierrmsg3(-1,23,"已斷線"); //未登入
//                      dierrmsg(0,"登出成功"); //登出成功
        break;

        /////////////////////*  功能:搜尋姓名並取得姓名與帳號
        case "search_name":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['partname'])){
                        if($_SESSION['identity']=='consumer'){
//                              $identity=$_SESSION['identity'];
                                $account=$_SESSION['account'];
                                $partname=$post['partname'];
//只在consumer_record查朋友資料
                                        $rs=SQL($db,"SELECT account,lastname,firstname FROM consumer WHERE lastname like  '".$partname."' or firstname like  '".$partname."' ");
                                        while($row=$rs->fetch(PDO::FETCH_ASSOC)){
                                                $array[]=array("account"=>$row['account'],"online"=>$row['online'],"lastname"=>$row['lastname'],"firstname"=>$row['firstname']);
                                        }
                                        if(!isset($array)) $array=array(); //consumer_record查無朋友資料//exchange_record查無朋友資料
                                        $arr[]=array("msg"=>0);
                                        $array=array_merge($arr,$array);
                                        echo json_encode($array);
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //未登入or資料未填寫完整
                }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入or資料未填寫完整
                }
        break;

        //////////////////*     功能:取得所有receiver新發送給sender的instant_message並將received設為1
        case "message_getnew":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
//              if(isset($post['rev_account'])){
                        if($_SESSION['identity']=='consumer'){
                                $account=$_SESSION['account'];
                                try{
//這樣會收到所有的人發給此人的所有訊息
                                        $receiver=$post['rev_account'];
                                        $rs=SQL($db,"SELECT * FROM instant_message WHERE (( receive_account='".$account."' and received=0 )) order by time ");
                                        $db->beginTransaction();
                                        while($row = $rs->fetch(PDO::FETCH_ASSOC)){
                                                $array[]=array("account"=>$row['send_account'],"message"=>$row['message'],"revaccount"=>$row['receive_account'],"time"=>$row['time']);
                                                $rs1=$db->exec("UPDATE instant_message set  received=1  WHERE ID='".$row['ID']."'");
                                        }
                                        $db->commit();
                                }
                                catch(PDOException $ex){
                                        $db->rollBack();
                                        dierrmsg(-1,"資料庫忙碌請重試");
                                }
                                if(!isset($array)) $array=array();
                                $arr[]=array("msg"=>0);
                                $array=array_merge($arr,$array);
                                echo json_encode($array);
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤//只有consumer能發//business另註冊消費者
//              }
//              else{
//                      dierrmsg(-1,"資料不可空白"); //資料未填寫完整
//              }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        ///////////////////*    功能:取得所有人新發送給sender的instant_message但不將received設為1            還會回傳個數count

        case "message_checknew":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
//              if(isset($post['rev_account'])){
                        if($_SESSION['identity']=='consumer'){
                                $account=$_SESSION['account'];
                                $count=0;
                                try{
//這樣會收到所有的人發給此人的所有訊息
                                        $receiver=$post['rev_account'];
                                        $rs=SQL($db,"SELECT * FROM instant_message WHERE  receive_account='".$account."' and received=0  order by time ");
                                        $db->beginTransaction();
                                        while($row = $rs->fetch(PDO::FETCH_ASSOC)){
                                                $count++;
                                                $array[]=array("account"=>$row['send_account'],"message"=>$row['message'],"revaccount"=>$row['receive_account'],"time"=>$row['time']);
                                        }
                                        $db->commit();
                                }
                                catch(PDOException $ex){
                                        $db->rollBack();
                                        dierrmsg(-1,"資料庫忙碌請重試");
                                }
                                if(!isset($array)) $array=array();
                                $arr[]=array("msg"=>0);
                                $arr1[]=array("count"=>$count);
                                $array=array_merge($arr,$arr1,$array);
                                echo json_encode($array);
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤//只有consumer能發//business另註冊消費者
//              }
//              else{
//                      dierrmsg(-1,"資料不可空白"); //資料未填寫完整
//              }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        /////////////*  功能:傳送即時訊息
        case "send_message":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['receive_account'])&&isset($post['message'])&&$_SESSION['account']!=$post['receive_account']){
                        if($_SESSION['identity']=='consumer' || $_SESSION['identity']=='business'){
                                $_SESSION['send_account']=$post['receive_account'];
                                $account=$_SESSION['account'];
                                $receive_account=$post['receive_account'];
                                $message=$post['message'];
//放在try內會造成當機,把它移出來試看看
                                        sendMqtt($receive_account, "message");
                                        try{
                                                $db->beginTransaction();
                                                if($_SESSION['identity']=='consumer'){
//會員發訊息給會員
                                                        $rs=$db->exec("INSERT INTO instant_message(send_account,receive_account,message) VALUES('".$account."','".$receive_account."','".$message."')");
                                                }
                                                else{
//店家發訊息給會員
//                                                      $rs1=$db->exec("select * from business where account='".$account."' ");
//                                                      while($row=$rs1->fetch(PDO::FETCH_ASSOC)){
//                                                              $rs=$db->exec("INSERT INTO instant_message(send_account,receive_account,message) VALUES('".$row['account']."', '".$receive_account."', '".$message."') ");
//                                                              $rs=$db->exec("INSERT INTO instant_message(send_account,receive_account,message) VALUES('店家', '".$receive_account."', '".$message."') ");
                                                                $rs=$db->exec("INSERT INTO instant_message(send_account,receive_account,message) VALUES('".$account."','".$receive_account."','".$message."')");
//                                                      }
                                                }
                                                $db->commit();
                                                dierr(0); //傳送成功
//                                              dierrmsg3(0,0,"傳送成功"); //傳送成功
                                        }
                                        catch(PDOException $ex){
                                                $db->rollBack();
                                                dierrmsg3(-1,21,"資料庫忙碌請重試");
                                        }
//以下不會被執行
                                $rs=CSQL($db,"SELECT count(*) FROM consumer WHERE account='".$receive_account."' and time>date_add(now(),interval -30 minute)");
                                if($rs==1){
                                }
                                else{
                                        $rs=$db->exec("UPDATE  consumer SET  online =  '0' WHERE account='".$receive_account."' and time<date_add(now(),interval -30 minute)"); //超過30分鐘未登入,更新為下線狀態
//                                      dierr(-1); //對方不在線上
                                }
                        }
                        else dierrmsg(-1,6,"軟體錯誤請用正版軟體"); //身份錯誤
                }
                else{
                        dierrmsg(-1,3,"資料不可空白"); //資料未填寫完整
                }
                }
                else{
                        dierrmsg(-1,20,"請重新登入"); //未登入
                }
        break;

        ////////////*   功能:取得所有sender與receiver互相通訊舊訊息instant_message的中received設為1者
        case "message_getall":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['rec_account'])){
                        if($_SESSION['identity']=='consumer'){
                                $identity=$_SESSION['identity'];
                                $account=$_SESSION['account'];
                                $receiver=$post['rec_account'];
                                $_SESSION['rec_account']=$receiver;
//在instant_message查資料
                                try{
                                        $db->beginTransaction();
                                        $rs=SQL($db,"SELECT * FROM instant_message WHERE ((( receive_account='".$account."' and send_account='".$receiver."' and received=1) OR ( receive_account='".$receiver."' and send_account='".$account."'  and received=1))and time>date_add(now(),interval -3 day))  order by time");
                                        while($row = $rs->fetch(PDO::FETCH_ASSOC)){
                                                $array[]=array("account"=>$row['send_account'],"message"=>$row['message'],"receiver"=>$row['receive_account'],"time"=>$row['time']);
//                                              $delete=$db->exec("UPDATE instant_message set  received=1  WHERE ID='".$row['ID']."'");
                                        }
                                        $db->commit();
                                }
                                catch(PDOException $ex){
                                        $db->rollBack();
                                        dierrmsg(-1,"資料庫忙碌請重試");
                                }
                                if(!isset($array)) $array=array();
                                $arr[]=array("msg"=>0);
                                $array=array_merge($arr,$array);
                                echo json_encode($array);
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //資料未填寫完整
                }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入or資料未填寫完整
                }
        break;

        ////////////////////*   功能:取得即時訊息,此設計缺少通訊對象
        case "receive_message":
                if(isset($_SESSION['identity']) && isset($_SESSION['account']) ){
                        if( $_SESSION['identity'] == 'consumer' ){
                                $account=$_SESSION['account'];
                                try{
                                        $db->beginTransaction();
//這樣會收到所有的人發給此人的所有訊息
                                        if(isset($_SESSION['send_account']) && $_SESSION['first']==1){
                                                $_SESSION['first']=0;   //login 配合receive_message
                                                $receiver=$_SESSION['send_account'];
                                                $rs=SQL($db,"SELECT * FROM instant_message WHERE (( receive_account='".$account."' and received=0 )
                                                                        OR ( receive_account='".$account."' and time>date_add(now(),interval -5 second) ))order by time ");
//                                                                      OR ( receive_account='".$account."' and send_account='".$receiver."' and time>date_add(now(),interval -5 hour) ) )order by time ");
//                                              $rs=SQL($db,"SELECT * FROM instant_message WHERE (( receive_account='".$account."' and received=0 ) OR ( receive_account='".$account."' and time>date_add(now(),interval -5 second) )) order by time ");
//                                              $rs=SQL($db,"SELECT * FROM instant_message WHERE ( receive_account='".$account."' and received=0 ) OR ( receive_account='".$account."' and time>date_add(now(),interval -5 second) ) order by time desc");
//                                              $rs=SQL($db,"SELECT * FROM instant_message WHERE receive_account='".$account."' and time>date_add(now(),interval -10 second) order by time desc");
                                        }
                                        elseif( $_SESSION['first']==1){
                                                $rs=SQL($db,"SELECT * FROM instant_message WHERE ( receive_account='".$account."' and received=0 ) order by time ");
                                        }
                                        else {
                                                $rs=SQL($db,"SELECT * FROM instant_message WHERE (( receive_account='".$account."' and received=0 ) OR ( receive_account='".$account."' and time>date_add(now(),interval -5 second) )) order by time ");
                                        }
//                                      if($_SESSION['first']==1){
//                                              $_SESSION['new']="";
//                                      }
                                        while($row = $rs->fetch(PDO::FETCH_ASSOC)){
                                                $pos =strpos($_SESSION['new'], $row['send_account'].":");
                                                if ( $pos === false){
                                                        $_SESSION['new']=$_SESSION['new'].$row['send_account'].": ";
//                                              if ( strpos($_SESSION['new'], $row['send_account'].":")){
//                                              }else{
                                                }
//                                                      $_SESSION['new']=$_SESSION['new'].",".$row['send_account'];
                                                if( $account != $row['send_account'] ){
//original
                                                        $array[]=array("account"=>$row['send_account'],"message"=>$row['message']);
//                                                      $array[]=array("account"=>$row['send_account'],"message"=>$row['message'],"add"=>$_SESSION['new']);
                                                }
                                                else{
                                                        $array[]=array("account"=>$row['receive_account'],"message"=>$row['message']);
                                                }
//造成APP錯誤
//                                              $array[]=array("account"=>$row['send_account'],"message"=>$row['message'],"revaccount"=>$row['receive_account'],"time"=>$row['time']);
//                                              $delete=$db->exec("DELETE FROM instant_message WHERE ID='".$row['ID']."'");
//修改received=1
//這樣太多次                    $delete=$db->exec("UPDATE instant_message set  received=1  WHERE ID='".$row['ID']."'");
//                                              $db->exec("INSERT INTO test(ID,send_account,receive_account,message) VALUES('".$row['ID']."','".$row['send_account']."','".$row['receive_account']."','".$row['message']."')");
                                        }
                                        if( $_SESSION['first']==1){
                                                $_SESSION['first']=0;   //login 配合receive_message
                                        }
//修改received=1
                                        $delete=$db->exec("UPDATE instant_message set  received=1  WHERE receive_account='".$account."' and received=0");
                                        $db->commit();
                                }
                                catch(PDOException $ex){
                                        $db->rollBack();
                                        dierrmsg3(-1, 21, "資料庫忙碌請重試");
                                }
                                if(!isset($array)) $array=array();
                                $arr[]=array("msg"=>0);
                                $array=array_merge($arr,$array);
                                echo json_encode($array);
                        }
                        else dierrmsg3(-1, 6, "軟體錯誤請用正版"); //身份錯誤//只有consumer能發//business另註冊消費者
                }
                else{
                        dierrmsg3(-1, 20, "請重新登入"); //未登入
                }
        break;

/******************************************************************************/
        //////////////////////* 功能:商家留言板 留言
        case "message_board":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['business_id'])&&isset($post['message'])){
                        if($_SESSION['identity']=='consumer'){
                                $account=$_SESSION['account'];
                                $business_id=$post['business_id'];
                                $message=$post['message'];
                                $check_id=CSQL($db,"SELECT count(*) FROM business WHERE ID='".$business_id."'");
                                if($check_id>0){
                                        $rs=$db->exec("INSERT INTO message_board(b_ID,post_account,message) VALUES('".$business_id."','".$account."','".$message."')");
                                        if($rs>0) dierrmsg(0,"留言成功"); //留言成功
                                        else dierrmsg(-1,"留言失敗"); //留言失敗
                                }
                                else{
                                        dierrmsg(-1,"查無此商家"); //查無此商家
                                }
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //資料未填寫完整
                }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        /////////////////////////*      功能:商家留言板 回覆
        case "re_message":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['board_id'])&&isset($post['message'])){
                        if($_SESSION['identity']=='consumer'||$_SESSION['identity']=='business'){
                                $account=$_SESSION['account'];
                                $board_id=$post['board_id'];
                                $message=$post['message'];
                                $check=ASQL($db,"SELECT * FROM message_board WHERE ID='".$board_id."'");
                                if($_SESSION['identity']=='business'){
                                        $check_account=CSQL($db,"SELECT account FROM business WHERE ID='".$check['b_ID']."'");
                                        if($account!=$check_account) dierrmsg(-1,"只能回覆自家留言板"); //商家只能回覆自家留言板
                                }
                                if($check['ID']>0){
                                        $rs=$db->exec("INSERT INTO re_message(board_id,re_account,message) VALUES('".$board_id."','".$account."','".$message."')");
                                        if($rs>0) dierr(0); //dierrmsg(0,"回覆成功"); //回覆成功
                                        else dierrmsg(-1,"回覆失敗"); //回覆失敗
                                }
                                else{
                                        dierrmsg(-1,"查無此留言"); //查無此留言
                                }
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //資料未填寫完整
                }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        ///////////////////*    功能:取得 商家留言板 主要留言
        case "get_board":
                if(isset($post['business_id'])&&isset($post['start'])&&isset($post['count'])){
                        $business_id=$post['business_id'];
                        $start=intval($post['start']);
                        $count=intval($post['count']);
                        $check_id=CSQL($db,"SELECT count(*) FROM business WHERE ID='".$business_id."'");
                        if($check_id>0){
                                $rs=SQL($db,"SELECT * FROM message_board where b_ID='".$business_id."' order by time desc limit ".$start.",".$count."");
                                while($row = $rs->fetch(PDO::FETCH_ASSOC)){
                                        $array[]=$row;
                                }
                                if(!isset($array)) $array=array(); //查無留言
                                $count=CSQL($db,"SELECT count(*) FROM message_board where b_ID='".$business_id."'");
                                $deals=CSQL($db,"SELECT deals FROM business where ID='".$business_id."'");
                                $arr[]=array("msg"=>0);
                                $arr[]=array("count"=>$count);
                                $arr[]=array("deals"=>$deals);
                                $array=array_merge($arr,$array);
                                echo json_encode($array);
                        }
                        else{
                                dierrmsg(-1,"查無此店家"); //查無此商家
                        }
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //資料未填寫完整
                }
        break;

        //////////////* 功能:取得 商家留言板 回覆留言
        case "get_reboard":
                if(isset($post['board_id'])&&isset($post['start'])&&isset($post['count'])){
                        $board_id=$post['board_id'];
                        $start=intval($post['start']);
                        $count=intval($post['count']);
                        $rs=SQL($db,"SELECT re_account,message,time FROM re_message where board_id='".$board_id."' order by time desc limit ".$start.",".$count."");
                        while($row = $rs->fetch(PDO::FETCH_ASSOC)){
                                $array[]=array("re_account"=>$row['re_account'],"message"=>$row['message'],"time"=>$row['time']);
                        }
                        if(!isset($array)) $array=array(); //查無留言
                        $count=CSQL($db,"SELECT count(*) FROM re_message where board_id='".$board_id."'");
                        $arr[]=array("msg"=>0);
                        $arr[]=array("count"=>$count);
                        $array=array_merge($arr,$array);
                        echo json_encode($array);
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //資料未填寫完整
                }
        break;

        ////////////////*       功能:取得商品列表
        case "get_commodity_list":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['start'])&&isset($post['count'])){
                        if($_SESSION['identity']=='business'||$_SESSION['identity']=='consumer'){
                                $start=intval($post['start']);
                                $count=intval($post['count']);
                                if(isset($post['id'])&&($_SESSION['identity']=='consumer'||$_SESSION['identity']=='business')){
                                        $business_id=$post['id'];
//會員用查詢,下架的無法看,刪除的無法看
                                $rs=SQL($db,"SELECT * FROM commodity WHERE b_ID='".$business_id."' and off_shelf='0' and del_record='0' order by time desc limit ".$start.",".$count."");
                                }
                                else if($_SESSION['identity']=='business'){
                                        $account=$_SESSION['account'];
                                        $business_id=CSQL($db,"SELECT ID FROM business WHERE account='".$account."'");
//店家用查詢,下架的可以看,刪除的無法看
                                        $rs=SQL($db,"SELECT * FROM commodity WHERE b_ID='".$business_id."' and  del_record='0' order by time desc limit ".$start.",".$count."");
                                }
                                else{
                                        dierrmsg(-1,"軟體錯誤請用正版"); //身分錯誤
                                }
//                              $rs=SQL($db,"SELECT * FROM commodity WHERE b_ID='".$business_id."' and off_shelf='1' order by time desc limit ".$start.",".$count."");
                                while($row = $rs->fetch(PDO::FETCH_ASSOC)){
                                        $array[]=array("ID"=>$row['ID'],"name"=>$row['name'],"detail"=>$row['detail'],"price"=>$row['price'],"path"=>$row['image'],"point"=>$row['point']);
                                }
                                if(!isset($array)) $array=array(); //查無商品
                                $count=CSQL($db,"SELECT count(*) FROM commodity WHERE b_ID='".$business_id."' and off_shelf='1'");
                                $arr[]=array("msg"=>0);
                                $arr[]=array("count"=>$count);
                                $array=array_merge($arr,$array);
                                echo json_encode($array);
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //資料未填寫完整
                }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        ///////////////*        功能:取得商品異動版本
        case "get_variation":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                        if($_SESSION['identity']=='business'){
                                $account=$_SESSION['account'];
                                $variation=CSQL($db,"SELECT commodity_variation FROM business WHERE account='".$account."'");
                                $array[]=array("msg"=>0);
                                $array[]=array("variation"=>$variation);
                                echo json_encode($array);
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        ///////////////*        功能:商家銷售額(全)
        case "sales_record":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                        if($_SESSION['identity']=='business'){
                                $account=$_SESSION['account'];
                                $id=CSQL($db,"SELECT ID FROM business WHERE account='".$account."'");
                                $recode=SQL($db,"SELECT `commodity`.`name`, `commodity`.`price`, `SS`.`sum(count)` sum  FROM `commodity`,( SELECT `com_ID`, sum(count) FROM `record_detail` WHERE `SID` IN (SELECT SID FROM `consumer_record` WHERE `b_ID`='".$id."') GROUP BY `com_ID` ) as SS WHERE `ID`=SS.com_ID");
                                while($row = $recode->fetch(PDO::FETCH_ASSOC)){
                                        $array[]=array("commodity_name"=>$row['name'],"price"=>$row['price'],"count"=>$row['sum']);
                                }
                                if(!isset($array)) $array=array(); //查無消費紀錄
                                $arr[]=array("msg"=>0);
                                $array=array_merge($arr,$array);
                                echo json_encode($array);
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        ///////////////*        功能:商家銷售額(年)
        case "sales_record_year":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['date'])){
                        if($_SESSION['identity']=='business'){
                                $account=$_SESSION['account'];
                                $date=$post['date']."-01";
                                $id=CSQL($db,"SELECT ID FROM business WHERE account='".$account."'");
                                $recode=SQL($db,"SELECT `commodity`.`name`, `commodity`.`price`, `SS`.`sum(count)` sum  FROM `commodity`,( SELECT `com_ID`, sum(count) FROM `record_detail` WHERE `SID` IN (SELECT SID FROM `consumer_record` WHERE `b_ID`='".$id."' and time>'".$date."' and time<date_add('".$date."',interval +1 year)) GROUP BY `com_ID` ) as SS WHERE `ID`=SS.com_ID");
                                while($row = $recode->fetch(PDO::FETCH_ASSOC)){
                                        $array[]=array("commodity_name"=>$row['name'],"price"=>$row['price'],"count"=>$row['sum']);
                                }
                                if(!isset($array)) $array=array(); //查無消費紀錄
                                $arr[]=array("msg"=>0);
                                $array=array_merge($arr,$array);
                                echo json_encode($array);
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //
                }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        /////////////////*      功能:商家銷售額(月)
        case "sales_record_month":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['date'])){
                        if($_SESSION['identity']=='business'){
                                $account=$_SESSION['account'];
                                $date=$post['date']."-01";
                                $id=CSQL($db,"SELECT ID FROM business WHERE account='".$account."'");
                                $recode=SQL($db,"SELECT `commodity`.`name`, `commodity`.`price`, `SS`.`sum(count)` sum  FROM `commodity`,( SELECT `com_ID`, sum(count) FROM `record_detail` WHERE `SID` IN (SELECT SID FROM `consumer_record` WHERE `b_ID`='".$id."' and time>'".$date."' and time<date_add('".$date."',interval +1 month)) GROUP BY `com_ID` ) as SS WHERE `ID`=SS.com_ID");
                                while($row = $recode->fetch(PDO::FETCH_ASSOC)){
                                        $array[]=array("commodity_name"=>$row['name'],"price"=>$row['price'],"count"=>$row['sum']);
                                }
                                if(!isset($array)) $array=array(); //查無消費紀錄
                                $arr[]=array("msg"=>0);
                                $array=array_merge($arr,$array);
                                echo json_encode($array);
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //
                }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        //////////////////////* 功能:商家銷售額(日)
        case "sales_record_day":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                        if($_SESSION['identity']=='business'){
                                $account=$_SESSION['account'];
                                $date=$post['date'];
                                $id=CSQL($db,"SELECT ID FROM business WHERE account='".$account."'");
                                $recode=SQL($db,"SELECT `commodity`.`name`, `commodity`.`price`, `SS`.`sum(count)` sum  FROM `commodity`,( SELECT `com_ID`, sum(count) FROM `record_detail` WHERE `SID` IN (SELECT SID FROM `consumer_record` WHERE `b_ID`='".$id."' and time>'".$date."' and time<date_add('".$date."',interval +1 day)) GROUP BY `com_ID` ) as SS WHERE `ID`=SS.com_ID");
                                while($row = $recode->fetch(PDO::FETCH_ASSOC)){
                                        $array[]=array("commodity_name"=>$row['name'],"price"=>$row['price'],"count"=>$row['sum']);
                                }
                                if(!isset($array)) $array=array(); //查無消費紀錄
                                $arr[]=array("msg"=>0);
                                $array=array_merge($arr,$array);
                                echo json_encode($array);
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        ///////////////////*    功能:取得商家優惠資訊
        case "get_deals":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                        if($_SESSION['identity']=='business'){
                                $account=$_SESSION['account'];
                                $deals=CSQL($db,"SELECT deals FROM business WHERE account='".$account."'");
                                $array[]=array("deals"=>$deals);
                                $arr[]=array("msg"=>0);
                                $array=array_merge($arr,$array);
                                echo json_encode($array);
                        }
                        else dierrmsg(-1,"錯誤請用正版軟體"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        /////////////////*      功能:消費者兌換商品
        case "redeemed":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['commodity_id'])){
//消費者才能兌換商品
                if($_SESSION['identity']=='consumer'){
                                $account=$_SESSION['account'];
                                $commodity_id=$post['commodity_id'];
//
                                $commodity=ASQL($db,"SELECT c1.ID com_ID, c2.ID c_ID, c1.time,c1.num_limit,c1.person_limit, c1.b_ID,c1.point,c2.consumer_point FROM commodity c1,consumer c2,business b1 WHERE c1.ID='".$commodity_id."' and c2.account='".$account."' and b1.ID=c1.b_ID");
                                if($commodity['consumer_point'] < $commodity['point']) dierrmsg(-1,"餘額點數不足"); //餘額點數不足
//商品限兌換數量,若已為0則不能兌換
                                if($commodity['num_limit'] < 1) dierrmsg(-1,"兌換限額已滿"); //兌換限額已滿
//檢查每人可兌換數量
                                if($commodity['person_limit'] > 0) {
                                        $count=CSQL($db,"SELECT count(*) FROM exchange_record WHERE com_ID='".$commodity_id."' and c_ID='".$commodity['c_ID']."' and time >'".$commodity['time']."'");
                                        if ($commodity['person_limit'] <= $count)                dierrmsg(-1,"每人兌換限額已滿"); //兌換限額已滿
                                }

                                try{
                                        $db->beginTransaction();
//檢查
//扣掉商品限兌換數量,若已為0則不能兌換
//扣掉消費者點數加入店家
                                        $transaction=$db->exec("UPDATE consumer t1,business t2 set t1.consumer_point=t1.consumer_point-".$commodity['point'].",t2.business_point=t2.business_point+".$commodity['point']." where t1.account='".$account."' and t2.ID='".$commodity['b_ID']."'");
                                        $transaction=$db->exec("UPDATE commodity set num_limit=num_limit-1 where  ID='".$commodity['com_ID']."' ");
//插入兌換記錄
                                        $keyrnd=GetPassword(mt_rand());
                                        $db->exec("INSERT INTO exchange_record ( c_ID, com_ID, b_ID, key_rnd, point) VALUES ('".$commodity['c_ID']."', '".$commodity['com_ID']."', '".$commodity['b_ID']."', '".$keyrnd."', '".$commodity['point']."')");
//                                      $db->exec("INSERT INTO exchange_record ( c_ID, com_ID, b_ID, key_rnd) VALUES ('".$commodity['c_ID']."', '".$commodity['com_ID']."', '".$commodity['b_ID']."', '".$keyrnd."')");
//插入核對用點數轉移記錄
                                        $db->exec("INSERT INTO transfer_record (c_ID, send_ID, identity, point) values ( '".$commodity['b_ID']."', '".$commodity['c_ID']."', 'exchange' , '".$commodity['point']."') ");
                                        $db->commit();
                                        dierr(0);
//                                      dierrmsg(0,"兌換成功");
                                }
                                catch(PDOException $ex){
                                        $db->rollBack();
                                        dierrmsg(-1,"資料庫忙碌請重試");
                                }
                        }
                        else dierrmsg(-1,"錯誤請用正版軟體"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //
                }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        /////////////////*      功能:消費者/商家 查看兌換列表
        case "get_redeemed_record":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                        $account=$_SESSION['account'];
                        if($_SESSION['identity']=='consumer'){
//只能查自己的兌換列表
                        $rs=SQL($db,"SELECT b1.business_name,c2.name,r1.confirm,r1.time FROM exchange_record r1,consumer c1,commodity c2,business b1 WHERE c1.account='".$account."' and c1.ID=r1.c_ID and c2.ID=r1.com_ID and b1.ID=c2.b_ID");
                                while($row=$rs->fetch(PDO::FETCH_ASSOC)){
                                        $array[]=$row;
                                }
                                if(!isset($array)) $array=array(); //查無消費者兌換紀錄
                                $arr[]=array("msg"=>0);
                                $array=array_merge($arr,$array);
                                echo json_encode($array);
                        }
                        else if($_SESSION['identity']=='business'){
//只能查自己的兌換列表
                                $rs=SQL($db,"SELECT c1.account,c2.name,r1.confirm,r1.time FROM exchange_record r1,consumer c1,commodity c2,business b1 WHERE c1.ID=r1.c_ID and c2.ID=r1.com_ID and b1.ID=c2.b_ID and b1.account='".$account."'");
                                while($row=$rs->fetch(PDO::FETCH_ASSOC)){
                                        $array[]=$row;
                                }
                                if(!isset($array)) $array=array(); //查無商家兌換紀錄
                                $arr[]=array("msg"=>0);
                                $array=array_merge($arr,$array);
                                echo json_encode($array);
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        //////////////////*     功能:商家查看單一消費者未兌換商品
        case "unredeemed_record":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['account'])){
                        if($_SESSION['identity']=='business'){
                                $account=$_SESSION['account'];
                                $consumer_account=$post['account'];
                                $rs=SQL($db,"SELECT r1.ID,c2.name FROM exchange_record r1,consumer c1,commodity c2,business b1 WHERE c1.ID=r1.c_ID and c2.ID=r1.com_ID and b1.ID=c2.b_ID and c1.account='".$consumer_account."' and b1.account='".$account."' and r1.confirm='0'");
                                while($row=$rs->fetch(PDO::FETCH_ASSOC)){
                                        $array[]=$row;
                                }
                                if(!isset($array)) $array=array(); //無商家兌換紀錄
                                $arr[]=array("msg"=>0);
                                $array=array_merge($arr,$array);
                                echo json_encode($array);
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //
                }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

        /////////////////*      功能:商家確認兌換
        case "confirm_redeemed":
                if(isset($_SESSION['identity'])&&isset($_SESSION['account'])){
                if(isset($post['record_id'])){
                        if($_SESSION['identity']=='business'){
                                $account=$_SESSION['account'];
                                $record_id=$post['record_id'];
                                try{
                                        $db->beginTransaction();
                                        $db->exec("UPDATE exchange_record SET confirm='1' WHERE ID='".$record_id."'");
                                        $db->commit();
                                        dierr(0);
//                                      dierrmsg(0,"兌換成功");
                                }
                                catch(PDOException $ex){
                                        $db->rollBack();
                                        dierrmsg(-1,"資料庫忙碌請重試");
                                }
                        }
                        else dierrmsg(-1,"軟體錯誤請用正版"); //身份錯誤
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //
                }
                }
                else{
                        dierrmsg(-1,"請重新登入"); //未登入
                }
        break;

///////////////////////////////////////////////////////////////////
        case "test":
//              $key=GetPassword(mt_rand());
//$PCNAME = getenv ("COMPUTERNAME");
//$PCNAME =$_SERVER['REMOTE_ADDR'];
$PCNAME1=gethostbyaddr($_SERVER['REMOTE_ADDR']);
$PCNAME2 =gethostname();
$browser=$_SERVER['HTTP_USER_AGENT'];
//////*
//////////取得使用者真實IP，即使使用Proxy
//////////$remote_ip是存放使用者IP的變數
/////////////
if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && isset($_SERVER['HTTP_VIA'])){
 $remote_ip =$_SERVER['HTTP_X_FORWARDED_FOR'];
}else{
 $remote_ip =$_SERVER['REMOTE_ADDR'];
}
//這些抓不到東西
$browser_all  = get_browser(null,true);
$browser_name = $browser_all["browser"];
$browser_version= $browser_all["version"];
$os= $browser_all["platform"];

        die('[{"msg":0} {"message":"'.$remote_ip.'"}  {"message_all":"'.$browser_all.'"} {"message_brow":"'.$browser_name.'"} {"message_ver":"'.$browser_version.'"}{"message0":"'.$PCNAME1.'"} {"message1":"'.$PCNAME2.'"} {"message2":"'.$browser.'"}]');

        ////////////////////////////////////
        $IMEI=GetPassword($post['IMEI']);
                $account=$post['account'];
//                              dierrmsg(-1,"請重新登入"); //未登入
                        $rs=$db->exec("UPDATE consumer SET  IMEI = '".$IMEI."' WHERE account='".$account."' ");
                        if($rs>0)
                                dierrmsg(0,"$IMEI"); //驗證正確
                        else
                                dierrmsg(-1,"請重新登入"); //未登入

        $arr=array();
                        if(!is_address($post['address'])) dierrmsg(-1,"不正確地址"); //不正確地址
        $address=$post['address'];
        $address=urlencode($address);
        $url="http://maps.googleapis.com/maps/api/geocode/json?address=".$address."&sensor=false&language=zh-TW";
        $content=file_get_contents($url);
        $json=json_decode($content);
        if(!strcasecmp(strval($json->status),"OK") and count($json->results)<4){
                $arr['address']=strval($json->results[0]->formatted_address);

                $arr['lat']=strval($json->results[0]->geometry->location->lat);
                $arr['lng']=strval($json->results[0]->geometry->location->lng);

                foreach($json->results[0]->address_components as $v){
                        switch($v->types[0]){
                                case    "administrative_area_level_2":
                                        //縣市
                                        $arr['city']=strval($v->long_name);
                                break;

                                case    "country":
                                        //國家
                                        $arr['country']=strval($v->long_name);
                                break;

                                case    "postal_code":
                                        //郵政編碼
                                        $arr['zipcode']=strval($v->long_name);
                                break;

                                case    "locality":
                                        //左營區
                                        $arr['locality']=strval($v->long_name);
                                break;
                        }
                } //end foreach
        if(isset($arr['zipcode']))
        die('[{"msg":0} {"message":"'.$arr['lng'].'"} {"message":"'.$arr['lat'].'"} {"message":"'.$arr['country'].'"} {"message":"'.$arr['zipcode'].'"}{"message":"'.$arr['city'].'"}{"message":"'.$arr['locality'].'"} {"message":"'.$arr['address'].'"}]');
        }
        dierrmsg(-1,"地址格式錯誤"); //地址格式錯誤
        //}

////////////////////////////////////////////////////////////
//      $line = "Vi is the greatest word processor ever created!";
// perform a case-Insensitive search for the word "Vi"
//      if (preg_match("/\bVi\b/is", $line, $match))
//      $line = $post['phone'];;
// perform a case-Insensitive search for the word "Vi"
//      if (preg_match("/^[0][1-9][0-9]{0,2}[-][0-9]{6,8}$/", $line, $match))
//              dierrmsg(0,$match[0],$match[1]);
//      else
//              dierrmsg(-1,"電話格式錯誤");

                if(isset($post['phone'])&&isset($post['IMEI'])){
                $phone=$post['phone'];
                $IMEI=$post['IMEI'];
//              if(!preg_match("/^[0][1-9][0-9]{0,2}[-][0-9]{6,8}$/", $phone) || strlen($phonenum) < 10 || strlen($phonenum) > 11){
//              dierrmsg(0,"驗證正確");
//      }
//              &&isset($post['lastname'])&&isset($post['firstname'])&&isset($post['birthday'])&&isset($post['city'])&&isset($post['address'])&&isset($post['email'])&&isset($post['phone'])&&isset($post['IMEI'])){
                        if(!is_phone($post['phone'])) dierrmsg(-1,"電話格式錯誤"); //電話格式錯誤
                        if(strlen($post['IMEI'])!=15) dierrmsg(-1,"IMEI碼格式錯誤"); //IMEI碼格式錯誤
                        dierrmsg(0,"驗證正確"); //驗證正確
                }
                else{
                        dierrmsg(-1,"資料不可空白"); //
                }

        break;
/*********************************************************************/
}
unset($db);
