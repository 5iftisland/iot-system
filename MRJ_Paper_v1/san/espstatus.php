<?php

$lifeTime = 3600 * 24 * 30;    ////// 30天
session_set_cookie_params($lifeTime);
require_once("config.php");
require_once ("phpMQTT.php");
header('Content-Type: text/html; charset=utf-8');

if(isset($post['espmac'])){
	
	$espmac=$post['espmac'];
	$esprow=ASQL($db,"SELECT ID,esptype,status FROM ESPModule WHERE espmac='".$espmac."'");
	if(isset($post['status'])){
		$status=$post['status'];
	}else{
		$status="null";
	}
	if(isset($post['status1'])){
		$status1=$post['status1'];
	}else{
		$status1="null";
	}
	if(isset($post['switchstatus'])){
		$switchstatus=$post['switchstatus'];
	}else{
		$switchstatus="null";
	}
	//控制器傳來的資料就直接更新狀態,感測器傳來的資料則是要進行邏輯運算判斷是否開關家電
	
	switch($esprow['esptype']){
		
		case "light":
		case "smartswitch":
			$rs=$db->exec("UPDATE ESPModule set status='".$status."',time = now() WHERE espmac='".$espmac."'");	
			$rs=$db->exec("INSERT INTO EspStateLog (ESP_ID,state,switchstatus) VALUES ('".$esprow['ID']."','".$status."','".$switchstatus."')");
		break;
		case "fan":
			//$rs=$db->exec("UPDATE ESPModule set status='".$esprow['status']."',time = now() WHERE espmac='".$espmac."'");	
		break;
		case "airconditioningcomplex":
		case "anemometersensor":
		case "temperaturesensor":
		case "luminositysensor":
			//$switchcontrol=$esprow['status']&$esprow['status'];
			$rs=$db->exec("UPDATE ESPModule set status='".$post['status']."',time = now() WHERE espmac='".$espmac."'");	
			$rs=$db->exec("INSERT INTO EspStateLog (ESP_ID,state,state1,switchstatus) VALUES ('".$esprow['ID']."','".$post['status']."','".$status1."','".$switchstatus."')");
		$EspID=CSQL($db,"SELECT ID FROM ESPModule WHERE espmac='".$espmac."'");
		$json=CSQL($db,"SELECT logic FROM LogicController WHERE ESP_ID='".$EspID."'");
		//$json = '{"IF":[{"sensor":"1a:fe:34:e0:3f:3c","condition":">","value":"100"}],"THEN":[{"appliance":"5e:cf:7f:81:b0:b5","output":"open"}],"ELSE":[{"appliance":"5e:cf:7f:81:b0:b5","output":"close"}]}';
	
		$obj = json_decode($json,true);
		//print $obj['IF']{'logic'};

		foreach ($obj as $key => $value) {//拆解 IF  THEN  ELSE 這一層
			foreach ($value as $value1) {//拆解 每一個JSON結構
			//echo "Key:".$value1.";".$value1."<br />\n";
				foreach ($value1 as  $key1=>$value2) {//拆解JSON內部KEY,Value的資料	
					echo "Key:".$key.";".$key1.":".$value2."<br />\n";		
					switch($key1){
						case "logic":
							$logicarr[]=$value2;//array($value2);
						break;
						case "sensor":
							$sensorarr[]=$value2;//array($value2);
						break;
						case "condition":
							$conditionarr[]=$value2;//array($value2);
						break;
						case "value":
							$valuearr[]=$value2;//array($value2);
						break;
						case "appliance":
							if($key=="ELSE")$ELSEApp[]=$value2;//array($value2);
							else $THENApp[]=$value2;//array($value2);
						break;
						case "output":
							if($key=="ELSE")$ELSEout[]=$value2;//array($value2);
							else $THENout[]=$value2;//array($value2);
						break;
						
					}
				} 
				//print_r(array_keys($value1));
				//echo "Key:".$key.";".$key1.":".array_search("logic", $value1)."<br />\n";	
				//echo "Key:".$key.";".$key1.":".$value2."<br />\n";	
			}
		}

		//============================判斷式============================
		//比較運算元
		
		for($x = 0; $x < count($conditionarr); $x++) {
			
			$Espstatus=CSQL($db,"SELECT status FROM ESPModule WHERE espmac='".$sensorarr[$x]."'");
			//$Espstatus=-1;
			
			switch($conditionarr[$x]){
				case ">":	$ifres[]=intval($Espstatus>$valuearr[$x]);	break;
				case "<":	$ifres[]=intval($Espstatus<$valuearr[$x]);	break;
				case "=": 	$ifres[]=intval($Espstatus==$valuearr[$x]);	break;
				case "~":	$ifres[]=intval($Espstatus!=$valuearr[$x]);	break;
			}
		}
		//邏輯運算元
		$autores=$ifres[0];
		for($i = 1; $i <= count($logicarr); $i++) {
			switch($logicarr[$i]){
				case "&":	$autores=$autores and $ifres[$i];	break;
				case "||":	$autores=$autores or $ifres[$i];	break;
				case "^": 	$autores=$autores xor $ifres[$i];	break;
		//		case "~":	$autores=$autores ! $ifres[$i];		break;
			}
		}
		$mqtt = new phpMQTT("ehome.estar.com.tw", 1883, "phpMQTT");
		//結果
		if($autores)
		{
			$OutputApp=$THENApp;
			$Output=$THENout;
			if ($mqtt->connect(true,NULL,"san","san123")){	
				for($x = 0; $x < count($Output); $x++) {
				
					$row=ASQL($db,"SELECT espacc,esptype,espname,espmac,espgroup,status from ESPModule where espmac='".$OutputApp[$x]."'");
					$g_ID=$row['espgroup'];
					$THEN=$THEN.",".$row['espname'].":".$Output[$x];
				}
				for($x = 0; $x < count($sensorarr); $x++) {
				
					$row=ASQL($db,"SELECT espacc,esptype,espname,espmac,espgroup,status from ESPModule where espmac='".$sensorarr[$x]."'");
					$IF=$IF.",".$row['espname'].$conditionarr[$x].$valuearr[$x];
					
				}
				$grouprow=CSQL($db,"SELECT name from group_name where ID='".$g_ID."'");
					$rs=SQL($db,"SELECT Admin_ID from group_member where g_ID='".$g_ID."'");//由群組ID 搜尋加入該群組之使用者ID			
						while($row = $rs->fetch(PDO::FETCH_ASSOC)){									
							$targetacc=CSQL($db,"SELECT account from Administrator where ID='".$row['Admin_ID']."'");//由群組的使用者ID 搜尋該ID之帳號		
							
							$mqtt->publish($targetacc, "warning=".substr($IF, 1)."=".substr($THEN,1));
									
					}
				
						//$acc=CSQL($db,"SELECT account from Administrator where ID='".$row['espacc']."'");//搜尋使用者ID
						//$topicstr=$acc."/".$grouprow."/".$row['espmac']."/".$row['espname']."/".$row['esptype']."/";
						//$mqtt->publish($topicstr, "action=".$Output[$x]);
				$mqtt->close();
			}		
		}
		else
		{
			$OutputApp=$ELSEApp;
			$Output=$ELSEout;
		}
					
		//輸出
		
		if ($mqtt->connect(true,NULL,"san","san123")){	
			for($x = 0; $x < count($Output); $x++) {
				
				$row=ASQL($db,"SELECT espacc,esptype,espname,espmac,espgroup,status from ESPModule where espmac='".$OutputApp[$x]."'");
				$grouprow=CSQL($db,"SELECT name from group_name where ID='".$row['espgroup']."'");
				if($row['status']!=$Output[$x]){//輸出與狀態不同則發送MQTT，相同就不發了
					$acc=CSQL($db,"SELECT account from Administrator where ID='".$row['espacc']."'");//搜尋使用者ID
					$topicstr=$acc."/".$grouprow."/".$row['espmac']."/".$row['espname']."/".$row['esptype']."/";
					$mqtt->publish($topicstr, "action=".$Output[$x]);
					echo $row['status']."===".$Output[$x];
				}
			}			
			$mqtt->close();
		}			
		break;
		case "remotecontrol":
			$EspID=CSQL($db,"SELECT ID FROM ESPModule WHERE espmac='".$espmac."'");
			$json=CSQL($db,"SELECT logic FROM LogicController WHERE ESP_ID='".$EspID."'");
			$obj=json_decode(stripslashes($json));//解析Json 資料
			foreach($obj as $key => $value) {
				//if($value) {
					
					$row1=ASQL($db,"SELECT status,espacc,esptype,espname,espmac,espgroup from ESPModule where espmac='".$value->{$post['status']}."'");
					$acc=CSQL($db,"SELECT account from Administrator where ID='".$row1['espacc']."'");//搜尋使用者ID
					$groupname=CSQL($db,"SELECT name from group_name where ID='".$row1['espgroup']."'");//搜尋使用者ID
					
					$topicstr=$acc."/".$groupname."/".$value->{$post['status']}."/".$row1['espname']."/".$row1['esptype']."/";
					echo $row1['status'];
					switch($row1['status']){
						case "on":$setoutput="off";break;
						case "off":$setoutput="on";break;
					}
					
					$mqtt = new phpMQTT("ehome.estar.com.tw", 1883, "phpMQTT");
					if ($mqtt->connect(true,NULL,"san","san123")){					
						$mqtt->publish($topicstr, "action=".$setoutput);
						$mqtt->close();
					}		
					$rs=$db->exec("UPDATE ESPModule set status='".$setoutput."',time = now() WHERE espmac='".$value->{$post['status']}."'");	
					
							
					
				//}
			}
			
		break;
	}


}
?>