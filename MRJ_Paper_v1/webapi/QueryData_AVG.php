<?php
	//ini_set('memory_limit', '1024M');
	include_once(dirname(dirname(__FILE__))."/config/mysql_config.php");
	$manager = new MongoDB\Driver\Manager("mongodb://localhost:27017");  
	//var_dump($manager);
	
	
	if(isset($_POST["mac"])&&isset( $_POST["start"])&&isset($_POST["end"])&&isset($_POST["sensor_type"])){
		$MAC= $_POST["mac"];
		$QueryTime_start= $_POST["start"];
		$QueryTime_end=$_POST["end"];
		$QuerySensor_type=$_POST["sensor_type"];
		//查詢
		$filter = ['date' => ['$lte' => $QueryTime_end,'$gte'=> $QueryTime_start],'mac' => $MAC,'sensor_type' => $QuerySensor_type];
		$options = [
			'projection' => ['_id' => 0],
			'sort' => ['sensor_type' => -1]
		];
		// 查询数据
		$query = new MongoDB\Driver\Query($filter,$options);
		$cursor = $manager->executeQuery('IOT.RowData', $query);
		
		
		$JsonArray=Array();
		$AVG_count=0;
		$AVG_time_old=0;
		$AVG_data_sum=0;
		foreach ($cursor as $document) {
			
			if(strtotime($document->time)-$AVG_time_old<=600){
				
				$AVG_data_sum+=$document->data;
				$AVG_count++;
			}else if(strtotime($document->time)-$AVG_time_old==strtotime($document->time)){
				$AVG_time_old=strtotime($document->time);
				
				$AVG_data_sum+=$document->data;
				$AVG_count++;			
			}
			else{
				
				$AVG_DATA=array("sensor_type"=>$document->sensor_type,"mac"=>$document->mac,"data"=>number_format($AVG_data_sum/$AVG_count, 2, '.', ''),"date"=>$document->date,"time"=>$document->time);
				$AVG_time_old=strtotime($document->time);
				
				$AVG_data_sum=$document->data;
				
				$JsonArray[]=$AVG_DATA;
				$AVG_count=1;
			}
			
		}
		
		echo json_encode($JsonArray);
		
	}else{
		echo "error";
	}
	
//projection 0不顯示他，1只顯示他
//'sort' => ['sensor_type' => -1],//其中 1 为升序排列，而-1是用于降序排列。

?>