/*
 *  Copyright (c) 2015 Taifatech Inc. All rights reserved.
 *  LoRa TLM926P01A module for Arudino
 *
 *  This example sent data(123456789) every ten seconds.
 *  This example TLM926P01A module baud rate should be set as 9600.
 *  TLM926P01A modules should set same parameter when data transfer.
 *  This example can use software serial output result, use pin8 as TX, pin9 as RX,
 *  and set baud rate 4800.
 *
 *  Author :   Terry <terry@taifatech.com>
 *  Date :      2015/11
 */
#include "TLM926P01A.h"
#include <SoftwareSerial.h>
#include <ArduinoJson.h>
#include "DHT.h"

#define DHTPIN 4     // what digital pin we're connected to
#define DHTTYPE DHT11   // DHT 11
DHT dht(DHTPIN, DHTTYPE);

char *data = "123456789";
int error = 0;

LoRaSetting lora_set, lora_read;
boolean write_success = false;


int read_data_len = 0;
char *local_buf;
int rssi ;


void setup()
{
	tlm_init();
	
	/* lora setting */
	lora_set.LoRaSF = 7;
	lora_set.LoRaPowerdBm =20;
	lora_set.LoRaEnableCCA = 0;
	lora_set.LoRaBandWidthKhz = 125;
	lora_set.LoRaDataRate = 4550;
	lora_set.LoRaWakeupPeriodms = 65;
	lora_set.LoRaUARTBaudRate = 9600;
	lora_set.LoRaFreqKHz = 916100;

	while(!write_success) {
		if (!tlm_write_parameters(&lora_set))
			write_success = true;
	}
	delay(1000);
	if (tlm_read_parameters(&lora_read))
		testprint(-1);
	else
		tlm_print_setting(&lora_read);
	
	dht.begin(); 
  pinMode(5, INPUT);  
   
}

void loop()
{
  int switchStatus = digitalRead(5);
  if(switchStatus==HIGH){
      float h = dht.readHumidity();
    float t = dht.readTemperature();
    if (isnan(h) || isnan(t)) {
      Serial.println("Failed to read from DHT sensor!");
      return;
    }else{
      String msg="";
      msg="0;4A30B001E5f44;1:";
      msg+=String(t);
      msg+=",2:";
      msg+=String(h);
      char buf[msg.length()+1];
      msg.toCharArray(buf, msg.length()+1);
      error = tlm_sent_data( buf , msg.length()) ;
    }
   }else{//
      String msg="";
      msg="1;4A30B001E5f44;1,0,100,'text',0;2,0,100,'text',0";
      char buf[msg.length()+1];
      msg.toCharArray(buf, msg.length()+1);
      error = tlm_sent_data( buf , msg.length()) ;
   }
	delay(1000);
}

