//#include <SoftwareSerial.h>
#include <DHT.h>
//SoftwareSerial Serial1(2, 3); // RX, TX
#define DHTTYPE DHT22
#define DHTPIN 2

#define FANPIN 11
#define ExhaustFanPIN 12

#define LEDR 8
#define LEDG 9
#define LEDB 10

long pmcf10=0;
long pmcf25=0;
long pmcf100=0;
long pmat10=0;
long pmat25=0;
long pmat100=0;

char buf[50];

const int PIRSensor = 3;     // 紅外線動作感測器連接的腳位
int sensorValue = 0;         // 紅外線動作感測器訊號變數

const int LightPin=0;
int LightValue;

DHT dht(DHTPIN, DHTTYPE);  
float temperature = 28.0; // assume current temperature. Recommended to measure with DHT22
float humidity = 25.0; // assume current humidity. Recommended to measure with DHT22

unsigned long time,oldtime;

String msg[2]={"",""};


void setup() {
// put your setup code here, to run once:
  Serial.begin(115200);
  Serial1.begin(9600);
  Serial3.begin(115200);
  pinMode(PIRSensor, INPUT);     
  pinMode(FANPIN, OUTPUT);    
  pinMode(ExhaustFanPIN, OUTPUT);    
  pinMode(LEDR, OUTPUT);    
  pinMode(LEDG, OUTPUT); 
  pinMode(LEDB, OUTPUT); 
  Break();
}

void loop() {
  // put your main code here, to run repeatedly:
  pms();
  PIR_Motion();
  light();
  _DHT();
  time = millis();
  if((time-oldtime)>30000){
    Serial3.print("3:");
    Serial3.print(pmcf25);
    Serial3.print(",");
    Serial3.print("4:");
    Serial3.print(pmat100);
    Serial3.print(",");
    Serial3.print("6:");
    Serial3.print(sensorValue);
    Serial3.print(",");
    Serial3.print("5:");
    Serial3.print(LightValue);
    Serial3.print(",");
    Serial3.print("1:");
    Serial3.print(temperature);
    Serial3.print(",");
    Serial3.print("2:");
    Serial3.println(humidity);

    Serial.print("3:");
    Serial.print(pmcf25);
    Serial.print(",");
    Serial.print("4:");
    Serial.print(pmat100);
    Serial.print(",");
    Serial.print("6:");
    Serial.print(sensorValue);
    Serial.print(",");
    Serial.print("5:");
    Serial.print(LightValue);
    Serial.print(",");
    Serial.print("1:");
    Serial.print(temperature);
    Serial.print(",");
    Serial.print("2:");
    Serial.println(humidity);
    oldtime=time;
  }
  if(Serial3.available() > 0){
      msg[0]="";
      msg[1]="";
      UartRead();
  }
   
  if(msg[0]=="16"){
      LEDStatus(msg[1]);
  }else if(msg[0]=="17"){
    _switch(FANPIN,msg[1]);
  }else if(msg[0]=="18"){
    _switch(ExhaustFanPIN,msg[1]);
  }
  delay(500);
}
void _switch(int PIN,String Status){
  if(Status=="1"){
    
      digitalWrite(PIN, HIGH);
    }else if(Status=="0"){
      digitalWrite(PIN, LOW);
   }
}
void LEDStatus(String Status){
  if(Status=="0"){
      Break();
  }else if(Status=="1"){
      White();
  }else if(Status=="2"){
      Chocolate2();
  }else if(Status=="3"){
      HotPink();
  }
}

void HotPink(){
  int r = 255;
  int g = 105;
  int b = 180;
  LightShow(r,g,b);
}
void Chocolate2(){
  int r = 238;
  int g = 118;
  int b = 33;
  LightShow(r,g,b);
}
void White(){
  int r = 255;
  int g = 255;
  int b = 255;
  LightShow(r,g,b);
}
void Break(){
  int r = 0;
  int g = 0;
  int b = 0;
  LightShow(r,g,b);
}
void LightShow(int r,int g,int b){
  analogWrite(LEDR, 255-r);
  analogWrite(LEDG, 255-g);
  analogWrite(LEDB, 255-b);
  delay(1000);
}

void  UartRead(){//serial讀取
 char c=' ';
 int i=0;
   do{
    if(Serial3.available() > 0){
      c=(char)Serial3.read();
      if(c!='\r'&& c!='\n'){    //排除\r與\n          
        if(c==','){
          i=1;
          }else{
            msg[i]+=c; 
          }
         
      }  
     }else{
        break;
     }
    }while (c!='\n'); //\r結尾字元
   
 }

void pms(){
   int count = 0;
   unsigned char c;
   unsigned char high;
   while (Serial1.available()) {
    c = Serial1.read();
    if((count==0 && c!=0x42) || (count==1 && c!=0x4d)){
      //Serial.println("check failed");
      break;
    }
    if(count > 15){
      //Serial.println("complete");
      break;
    }
    else if(count == 4 || count == 6 || count == 8 || count == 10 || count == 12 || count == 14) high = c;
    else if(count == 5){
      pmcf10 = 256*high + c;
      //Serial.print("CF=1, PM1.0=");
      //Serial.print(pmcf10);
      //Serial.println(" ug/m3");
    }
    else if(count == 7){
      pmcf25 = 256*high + c;
      //Serial.print("CF=1, PM2.5=");
     // Serial.print(pmcf25);
     // Serial.println(" ug/m3");
    }
    else if(count == 9){
      pmcf100 = 256*high + c;
      //Serial.print("CF=1, PM10=");
      //Serial.print(pmcf100);
      //Serial.println(" ug/m3");
    }
    else if(count == 11){
      pmat10 = 256*high + c;
      //Serial.print("atmosphere, PM1.0=");
      //Serial.print(pmat10);
      //Serial.println(" ug/m3");
    }
    else if(count == 13){
      pmat25 = 256*high + c;
      //Serial.print("atmosphere, PM2.5=");
      //Serial.print(pmat25);
      //Serial.println(" ug/m3");
    }
    else if(count == 15){
      pmat100 = 256*high + c;
      //Serial.print("atmosphere, PM10=");
      //Serial.print(pmat100);
      //Serial.println(" ug/m3");
    }
    count++;
  }
  while(Serial1.available()) Serial1.read();
}
void PIR_Motion(){
   // 讀取 PIR Sensor 的狀態
 int  PIR_Value = digitalRead(PIRSensor);

  // 判斷 PIR Sensor 的狀態
  if (PIR_Value == HIGH) {     
    //digitalWrite(ledPin, HIGH);  // 有人，開燈
    sensorValue=1;
  } 
  else {
    //digitalWrite(ledPin, LOW);   // 沒人，關燈
    sensorValue=0;
  }
 
}
void light(){
   LightValue = analogRead(LightPin);
  //Serial.println(LightValue);
 }

 void _DHT(){
  humidity = dht.readHumidity();
  temperature = dht.readTemperature();
  if (isnan(humidity) || isnan(temperature))
  {
    //Serial.println("Failed to read from DHT sensor!");   
    humidity=20;
    temperature=20;
    //delay(500); 
  }
  //Serial.println(humidity);
  //Serial.println(temperature);
 }
