
#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266WebServer.h>
#include <MQTT.h>
#include <ArduinoJson.h>
#include <WiFiManager.h>

ESP8266WebServer server(80);
//WiFiManager wifiManager;

void myDataCb(String& topic, String& data);
void myPublishedCb();
void myDisconnectedCb();
void myConnectedCb();
String UartRead();
String* split(String message,char ch);
void spilt(String message,char ch);
#define CLIENT_ID "18:FE:34:E0:3F:90"
// create MQTT object
MQTT myMqtt(CLIENT_ID, "120.114.134.135", 1883);

void AP_mode_config(){
  /* Set these to your desired credentials. */
  const char *ssid = "ESPap";
  const char *password = "123456";
 
  //wifiManager.startConfigPortal(ssid,password);
  WiFi.softAP(ssid, password);//開啟ap mode
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
 }
void Mqtt_config(){
     myMqtt.setUserPwd("mqtt","mqtt");
     // setup callbacks
     myMqtt.onConnected(myConnectedCb);
     myMqtt.onDisconnected(myDisconnectedCb);
     myMqtt.onPublished(myPublishedCb);
     myMqtt.onData(myDataCb);
     myMqtt.connect();
     String SUB_TOPIC="device/control/"+WiFi.macAddress();     
     Serial.println("subscribe to topic...");
     myMqtt.subscribe(SUB_TOPIC);
}
void Mqtt_DevRegInfo(String account,String gid){//Device Registration Information
    String topic="device/reg/"+WiFi.macAddress();
    String message;//(value);
    StaticJsonBuffer<200> jsonBuffer;
    JsonObject& root = jsonBuffer.createObject(); 
    JsonArray& data = root.createNestedArray("SensorItems");
    //data.add("21,0,1,'button1',0");//SensorItem num,min,max
    data.add("8,0,100,'text',0");
    data.add("9,0,100,'text',0");
    data.add("10,0,10000,'text',0");
    //data.add("4,0,10000,'text',0");
    //data.add("5,0,1024,'text',0");
    //data.add("6,0,1024,'text',0");
    //data.add("16,0,10000,'text',0");
    //data.add("17,0,10000,'text',0");
    //data.add("18,0,10000,'text',0");
    root["mac"] =WiFi.macAddress();
    root["account"] =account;
    root["gid"] =gid;
    root["mode"] ="wifi";
    root.printTo(message);
    // publish value to topic
    boolean result = myMqtt.publish(topic, message);
  }

void wifi_mode_switching(){
   int i=0;
   
  while (WiFi.status() != WL_CONNECTED && i<20) {
     delay(500);
     Serial.print(".");
     i++;
  }
  if(i>=20){
     //WiFi.mode(WIFI_AP_STA);
    //WiFi.enableAP(true);
     Serial.print("AP");
     WiFi.disconnect();
     AP_mode_config();
   }else{
     Serial.print("STA");
      Mqtt_config();
      //WiFi.softAPdisconnect(false);//關閉ap mode
   }
}
//wifiset page, also called for disconnect
void handleWifiset(){
  String msg;
  String content = "";  
  if (server.hasArg("ssid") && server.hasArg("passwd") && server.hasArg("account")){
    if (server.arg("ssid") != "" &&  server.arg("passwd") != "" && server.arg("ssid") != NULL &&  server.arg("passwd") != NULL && server.arg("account") != ""  && server.arg("account") != NULL && server.arg("gid") != ""  && server.arg("gid") != NULL ){
       String wifi_ssid_str= server.arg("ssid");
       char wifi_ssid[wifi_ssid_str.length()+1];
       wifi_ssid_str.toCharArray(wifi_ssid,wifi_ssid_str.length()+1);
       
       String wifi_passwd_str= server.arg("passwd");
       char wifi_password[wifi_ssid_str.length()+1];
       wifi_passwd_str.toCharArray(wifi_password,wifi_passwd_str.length()+1);
//       Serial.println(wifi_ssid);
//       Serial.println(wifi_password);
       WiFi.begin(wifi_ssid,wifi_password);//wifi連接
      
       content = "<html><body><h2>";
       content += "WIFI setting completed</h2></body></html>";
       server.send(200, "text/html", content);   
       Serial.println("Log in Successful");
       wifi_mode_switching();

       String account=server.arg("account");
       String gid=server.arg("gid");
       Mqtt_DevRegInfo(account,gid);
       return;
    }
     msg = "Wrong username/password! try again.";
     Serial.println("Log in Failed");
  }
  content = "<html><body><h2>";
  content +="Parameter Error";
  content += "</h2></body></html>";
//  content = "<html><body><form action='/wifiset' method='POST'>Please enter the WIFI to be connected<br>";
//  content += "SSID:<input type='text' name='ssid' placeholder='YOU WIFI SSID'><br>";
//  content += "Password:<input type='password' name='passwd' placeholder='YOU WIFI password'><br>";
//  content += "<input type='submit' name='SUBMIT' value='Submit'></form>" + msg + "<br>";
//  content += "You also can go <a href='/inline'>here</a></body></html>";
  server.send(200, "text/html", content);
}

//root page can be accessed only if authentification is ok
void handleRoot(){
  Serial.println("Enter handleRoot");
  String header;
  server.sendHeader("Location","/wifiset");
  server.sendHeader("Cache-Control","no-cache");
  server.send(301);
}

//no need authentification
void handleNotFound(){
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}
String UartRead(){//serial讀取
 char c=' ';
 String msg="";
   do{
    if(Serial.available() > 0){
      c=(char)Serial.read();
      if(c!='\r'&& c!='\n'){    //排除\r與\n          
        msg+=c; 
      }  
     }else{
        break;
     }
    }while (c!='\n'); //\r結尾字元
    Serial.println(msg);
    return msg;
 }
 
void setup() {
  //WiFi.disconnect();
  delay(1000);
  Serial.begin(115200);
  pinMode(13, OUTPUT);
  Serial.println(WiFi.macAddress());
  Serial.print("Configuring access point...");
  /* You can remove the password parameter if you want the AP to be open. */
  WiFi.begin("KSUIE_I", "ksuie4010");
  wifi_mode_switching();
  
  server.on("/", handleRoot);
  server.on("/wifiset", handleWifiset);
  server.on("/inline", [](){
    server.send(200, "text/plain", "this works without need of authentification");
  });
  server.onNotFound(handleNotFound);
  //here the list of headers to be recorded
  const char * headerkeys[] = {"User-Agent","Cookie"} ;
  size_t headerkeyssize = sizeof(headerkeys)/sizeof(char*);
  //ask server to track these headers
  server.collectHeaders(headerkeys, headerkeyssize );
  server.begin();
  Serial.println("HTTP server started");
}

unsigned long oldtime;
void loop() {
  server.handleClient();
  
    String Data=UartRead();
    String topic="device/RowData/"+WiFi.macAddress();
    if(Data!=""){
      StaticJsonBuffer<200> jsonBuffer;
      JsonObject& root = jsonBuffer.createObject(); 
      String valueStr;//(value);
       root["data"] =Data;
       root["mac"] =WiFi.macAddress();
       root.printTo(valueStr);
        // publish value to topic
        boolean result = myMqtt.publish(topic, valueStr);  
     }
  delay(1000);
}

void myConnectedCb()
{
  //Serial.println("connected to MQTT server");
}

void myDisconnectedCb()
{
 // Serial.println("disconnected. try to reconnect...");
  //delay(500);
  myMqtt.connect();
}

void myPublishedCb()
{
  //Serial.println("published.");
}

void myDataCb(String& topic, String& data)
{
  Serial.println(data);
//  String reMAC,reValue; 
//  int index=0;
//  index= data.indexOf(",");
//  reMAC=data.substring(0,index);
//  reValue=data.substring(index+1);   
//
//  if(reMAC==WiFi.macAddress()) {
//    
//   if(reValue.toFloat()==1){
//      digitalWrite(13, HIGH);
//      
//    }else if(reValue.toFloat()==0){
//      digitalWrite(13, LOW); 
//    } 
//    boolean result = myMqtt.publish("device/RowData/"+reMAC, reValue);
//  }
}
